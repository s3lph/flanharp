package me.s3lph.flanharp.dataproc

import android.content.Context
import androidx.core.app.NotificationCompat
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.Notifier
import me.s3lph.flanharp.R
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.Fahrplan
import java.lang.ref.WeakReference

class UpdateFahrplanTask(private val context: WeakReference<Context>,
                         callback: (() -> Unit)? = null) :
    FahrplanTask(context, callback) {

  override fun postprocess(fahrplan: Fahrplan, angelshifts: Angelshifts?) {
    context.get()?.let { context ->
      FHApplication.getInstance()?.database?.updateFahrplan(fahrplan, angelshifts)?.let { events ->
        if (events.isNotEmpty()) {
          Notifier.getInstance().let { notifier ->
            val notificationId = notifier.generateId()
            val builder = NotificationCompat.Builder(context, Notifier.CHANNEL_BACKGROUND)
            builder.setContentTitle(context.getString(R.string.cancelled_events))
                .setContentText(events.fold("") { xs, x -> xs + x + "\n" })
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setShowWhen(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
            notifier.manager?.notify(notificationId, builder.build())
          }
        }
      }
    }
  }

}