package me.s3lph.flanharp.dataproc

import android.content.Context
import android.os.AsyncTask
import androidx.core.app.NotificationCompat
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.Notifier
import me.s3lph.flanharp.R
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.Fahrplan
import java.io.InterruptedIOException
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

abstract class FahrplanTask(private val context: WeakReference<Context>,
                            private val callback: (() -> Unit)? = null) :
    AsyncTask<String, Unit, Unit>() {

  private var canCancel = AtomicBoolean(true)

  private var taskId: AtomicInteger? = null

  override fun doInBackground(vararg params: String?) {
    try {
      val fahrplanUrl = if (params.isNotEmpty()) params[0] else return
      val angelshiftsUrl = if (params.size >= 2) params[1] else null
      context.get()?.let { context ->
        Notifier.getInstance().let { notifier ->
          val notificationId = notifier.generateId()
          try {
            val builder = NotificationCompat.Builder(context, Notifier.CHANNEL_BACKGROUND)
            val (tid, cancelPendingIntent) = FHApplication.getInstance()?.registerTask(this)
                ?: throw IllegalStateException()
            taskId = AtomicInteger(tid)
            builder.setOngoing(true)
                .setContentTitle(context.getString(R.string.update_fahrplan_notification_title))
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setContentText(context.getString(R.string.update_fahrplan_notification_text_downloading))
                .addAction(NotificationCompat.Action(0, context.getString(R.string.cancel), cancelPendingIntent))
            notifier.manager?.notify(notificationId, builder.build())

            val fpConnection = try {
              val connection = URL(fahrplanUrl).openConnection() as HttpURLConnection
              connection.connect()
              connection
            } catch (e: InterruptedIOException) {
              throw InterruptedException()
            } catch (e: InterruptedException) {
              throw e
            } catch (e: Exception) {
              e.printStackTrace()
              builder.setOngoing(false)
                  .setSmallIcon(android.R.drawable.stat_notify_error)
                  .setContentText(context.getString(R.string.update_fahrplan_notification_text_failed, e.localizedMessage))
                  .mActions.clear()
              notifier.manager?.notify(notificationId, builder.build())
              return
            }
            val fahrplan = try {
              FahrplanJsonParser.parse(fpConnection.inputStream, fahrplanUrl
                  ?: throw NullPointerException()) ?: throw NullPointerException()
            } catch (e: InterruptedIOException) {
              fpConnection.disconnect()
              throw InterruptedException()
            } catch (e: InterruptedException) {
              fpConnection.disconnect()
              throw e
            } catch (e: Exception) {
              e.printStackTrace()
              fpConnection.disconnect()
              builder.setOngoing(false)
                  .setSmallIcon(android.R.drawable.stat_notify_error)
                  .setContentText(context.getString(R.string.update_fahrplan_notification_text_parsing_failed, e.localizedMessage))
                  .mActions.clear()
              notifier.manager?.notify(notificationId, builder.build())
              return
            }
            fpConnection.disconnect()

            var angelshifts: Angelshifts? = null
            angelshiftsUrl?.let { angelshiftsUrl ->

              val asConnection = try {
                val connection = URL(angelshiftsUrl)
                    .openConnection() as HttpURLConnection
                connection.connect()
                connection
              } catch (e: InterruptedIOException) {
                throw InterruptedException()
              } catch (e: InterruptedException) {
                throw e
              } catch (e: Exception) {
                e.printStackTrace()
                builder.setOngoing(false)
                    .setSmallIcon(android.R.drawable.stat_notify_error)
                    .setContentText(context.getString(R.string.update_fahrplan_notification_text_failed, e.localizedMessage))
                    .mActions.clear()
                notifier.manager?.notify(notificationId, builder.build())
                return
              }

              angelshifts = try {
                AngelshiftsJsonParser.parse(asConnection.inputStream, angelshiftsUrl)
                    ?: throw NullPointerException()
              } catch (e: InterruptedIOException) {
                asConnection.disconnect()
                throw InterruptedException()
              } catch (e: InterruptedException) {
                asConnection.disconnect()
                throw e
              } catch (e: Exception) {
                e.printStackTrace()
                asConnection.disconnect()
                builder.setOngoing(false)
                    .setSmallIcon(android.R.drawable.stat_notify_error)
                    .setContentText(context.getString(R.string.update_fahrplan_notification_text_parsing_failed, e.localizedMessage))
                    .mActions.clear()
                notifier.manager?.notify(notificationId, builder.build())
                return
              }
              asConnection.disconnect()

            }

            builder.setContentText(context.getString(R.string.update_fahrplan_notification_text_saving))
                .mActions.clear()
            notifier.manager?.notify(notificationId, builder.build())
            canCancel.set(false)

            try {
              postprocess(fahrplan, angelshifts)
            } catch (e: InterruptedException) {
              throw e
            } catch (e: Exception) {
              e.printStackTrace()
              builder.setOngoing(false)
                  .setSmallIcon(android.R.drawable.stat_notify_error)
                  .setContentText(context.getString(R.string.update_fahrplan_notification_text_saving_failed, e.localizedMessage))
                  .mActions.clear()
              notifier.manager?.notify(notificationId, builder.build())
              return
            }

            builder.setOngoing(false)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setContentText(
                    context.getString(R.string.update_fahrplan_notification_text_done,
                        fahrplan.conference.acronym))
                .mActions.clear()
            notifier.manager?.notify(notificationId, builder.build())

          } catch (ignored: InterruptedException) {
            notifier.manager?.cancel(notificationId)
          }
        }
      }
    } catch (e: InterruptedException) {
      e.printStackTrace()
    }
  }

  override fun onPostExecute(result: Unit?) {
    taskId?.get()?.let { taskId -> FHApplication.getInstance()?.unregisterTask(taskId) }
    callback?.invoke()
  }

  abstract fun postprocess(fahrplan: Fahrplan, angelshifts: Angelshifts? = null)

  fun requestCancel() {
    if (canCancel.get()) {
      this.cancel(true)
    }
  }

}