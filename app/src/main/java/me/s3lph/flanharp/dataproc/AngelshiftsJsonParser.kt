package me.s3lph.flanharp.dataproc

import android.net.Uri
import android.util.JsonReader
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.Event
import me.s3lph.flanharp.fahrplan.Person
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*

class AngelshiftsJsonParser {

  companion object {

    fun parse(stream: InputStream, source: String): Angelshifts? {
      val reader = JsonReader(InputStreamReader(stream))
      val shifts = parseShiftList(reader)
      reader.close()
      return Angelshifts(shifts, source)
    }

    private fun parseShiftList(reader: JsonReader): List<Event> {
      val list = mutableListOf<Event>()
      reader.beginArray()
      while (reader.hasNext()) {
        list.add(parseShift(reader))
      }
      reader.endArray()
      return list
    }

    private fun parseShift(reader: JsonReader): Event {
      var id: Int? = null
      var title: String? = null
      var description: String? = null
      var start: Date? = null
      var end: Date? = null
      var type: String? = null
      var place: String? = null
      var url: Uri? = null
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "id" -> id = -reader.nextInt() //Negative ID to avoid confusion with talk IDs
          "title" -> title = reader.nextString()
          "description" -> description = reader.nextString()
          "start" -> start = Date(reader.nextLong() * 1000L)
          "end" -> end = Date(reader.nextLong() * 1000L)
          "name" -> type = reader.nextString()
          "Name" -> place = reader.nextString()
          "map_url" -> url = Uri.parse(reader.nextString())
          else -> reader.skipValue()
        }
      }
      reader.endObject()
      return Event(id ?: throw NullPointerException(),
          title ?: throw NullPointerException(),
          null,
          null,
          description ?: throw NullPointerException(),
          0, //TODO
          start ?: throw NullPointerException(),
          end ?: throw NullPointerException(),
          type ?: throw NullPointerException(),
          place ?: throw NullPointerException(),
          url ?: throw NullPointerException(),
          false,
          null,
          Angelshifts.TYPESTRING,
          listOf(Person.YOU))
    }

  }

}