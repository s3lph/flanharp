package me.s3lph.flanharp.dataproc

import android.content.Context
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.Fahrplan
import java.lang.ref.WeakReference

class CreateFahrplanTask(context: WeakReference<Context>, callback: (() -> Unit)? = null) :
    FahrplanTask(context, callback) {

  override fun postprocess(fahrplan: Fahrplan, angelshifts: Angelshifts?) {
    FHApplication.getInstance()?.database?.insertOrReplaceFahrplan(fahrplan, angelshifts)
  }

}