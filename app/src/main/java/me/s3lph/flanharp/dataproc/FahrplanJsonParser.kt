package me.s3lph.flanharp.dataproc

import android.net.Uri
import android.util.JsonReader
import android.util.JsonToken
import android.util.Log
import me.s3lph.flanharp.fahrplan.Conference
import me.s3lph.flanharp.fahrplan.Event
import me.s3lph.flanharp.fahrplan.Fahrplan
import me.s3lph.flanharp.fahrplan.Person
import org.xmlpull.v1.XmlPullParser
import java.io.InputStream
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

class FahrplanJsonParser {

  companion object {

    private val dateTimeFormat: SimpleDateFormat by lazy {
      SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH)
    }
    private val dateFormat: SimpleDateFormat by lazy {
      SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    }

    fun parse(stream: InputStream, source: String): Fahrplan? {
      val reader = JsonReader(InputStreamReader(stream))
      reader.beginObject()
      if (!reader.hasNext()) {
        throw IllegalStateException()
      }
      if (reader.nextName() != "schedule") {
        throw IllegalStateException()
      }
      val schedule = parseSchedule(reader, source)
      reader.endObject()
      return schedule
    }

    private fun parseOptionalString(reader: JsonReader): String? {
      if (reader.peek() == JsonToken.STRING) {
        return reader.nextString()
      }
      reader.nextNull()
      return null
    }

    private fun parseDate(timestamp: String): Date {
      var date: Date?
      val format = when {
        timestamp.contains('T') -> dateTimeFormat
        else -> dateFormat
      }
      date = format.parse(timestamp)
      return date
    }

    private fun parseSchedule(reader: JsonReader, source: String): Fahrplan {
      var version: String? = null
      var conference: Conference? = null
      val events = mutableListOf<Event>()
      val persons = mutableMapOf<Int, Person>()
      var dayAnchor = Date()
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "version" -> version = reader.nextString()
          "conference" -> {
            val (c, da) = parseConference(reader, source, events, persons)
            conference = c
            dayAnchor = da
          }
          else -> reader.skipValue()
        }
      }
      reader.endObject()
      return Fahrplan(
          source,
          version ?: throw IllegalStateException(),
          conference ?: throw IllegalStateException(),
          dayAnchor,
          events,
          persons.values.toList())
    }

    private fun parseConference(reader: JsonReader, url: String, events: MutableList<Event>, persons: MutableMap<Int, Person>): Pair<Conference, Date> {
      var title: String? = null
      var acronym: String? = null
      var start: Date? = null
      var end: Date? = null
      var dayAnchor = Date()
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "title" -> title = reader.nextString()
          "acronym" -> acronym = reader.nextString()
          "start" -> start = parseDate(reader.nextString())
          "end" -> end = parseDate(reader.nextString())
          "days" -> {
            reader.beginArray()
            while (reader.hasNext()) {
              val (da, _events) = parseDay(reader, persons)
              dayAnchor = da
              events.addAll(_events)
            }
            reader.endArray()
          }
          else -> reader.skipValue()
        }
      }
      reader.endObject()
      return Pair(Conference(
          title ?: throw IllegalStateException(),
          acronym,
          start ?: throw IllegalStateException(),
          end ?: throw IllegalStateException(),
          start, //Dummy date, not used
          url, null), dayAnchor)
    }

    private fun parseDay(reader: JsonReader, persons: MutableMap<Int, Person>): Pair<Date, List<Event>> {
      val events: MutableList<Event> = mutableListOf()
      var dayId = 0
      var dayStart = Date()
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "index" -> dayId = reader.nextInt()
          "day_start" -> {
            dayStart = parseDate(reader.nextString())
          }
          "rooms" -> {
            reader.beginObject()
            while (reader.hasNext()) {
              reader.nextName()
              events.addAll(parseRoom(reader, dayId, persons))
            }
            reader.endObject()
          }
          else -> reader.skipValue()
        }
      }
      reader.endObject()
      val cal = Calendar.getInstance()
      cal.time = dayStart
      cal.add(Calendar.DAY_OF_MONTH, -(dayId - 1))
      val dayAnchor = cal.time
      return Pair(dayAnchor, events)
    }

    private fun parseRoom(reader: JsonReader, day: Int, persons: MutableMap<Int, Person>): List<Event> {
      val events: MutableList<Event> = mutableListOf()
      reader.beginArray()
      while (reader.hasNext()) {
        events.add(parseEvent(reader, day, persons))
      }
      reader.endArray()
      return events
    }

    private fun parseEvent(reader: JsonReader, day: Int, allPersons: MutableMap<Int, Person>): Event {
      var eventId = -1
      var title: String? = null
      var subtitle: String? = null
      var abstract: String? = null
      var description: String? = null
      var start: Date? = null
      var durationHour = 0
      var durationMin = 0
      var type: String? = null
      var track: String? = null
      var room: String? = null
      var url: Uri? = null
      var recorded: Boolean = true
      var slug: String? = null
      val persons = mutableListOf<Person>()
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "id" -> eventId = reader.nextInt()
          "title" -> title = reader.nextString()
          "subtitle" -> subtitle = parseOptionalString(reader) ?: ""
          "abstract" -> abstract = parseOptionalString(reader) ?: ""
          "description" -> description = parseOptionalString(reader) ?: ""
          "date" -> start = parseDate(reader.nextString())
          "duration" -> {
            reader.nextString()?.split(":")?.let {
              if (it.size == 2) {
                durationHour = it[0].toInt()
                durationMin = it[1].toInt()
              }
            }
          }
          "type" -> type = reader.nextString()
          "track" -> track = parseOptionalString(reader)
          "room" -> room = reader.nextString()
          "url" -> url = Uri.parse(reader.nextString())
          "do_not_record" -> recorded = !reader.nextBoolean()
          "slug" -> slug = reader.nextString()
          "persons" -> {
            reader.beginArray()
            while (reader.hasNext()) {
              val person = parsePerson(reader)
              allPersons[person.id] = person
              persons.add(person)
            }
            reader.endArray()
          }
          else -> reader.skipValue()
        }
      }
      reader.endObject()

      val calendar = GregorianCalendar()
      calendar.time = start ?: throw IllegalStateException()
      calendar.add(Calendar.HOUR, durationHour)
      calendar.add(Calendar.MINUTE, durationMin)
      val end = calendar.time
      return Event(
          eventId,
          title ?: throw IllegalStateException(),
          subtitle,
          abstract,
          description,
          day,
          start ?: throw IllegalStateException(),
          end ?: throw IllegalStateException(),
          type ?: "",
          room ?: throw IllegalStateException(),
          url,
          recorded,
          slug,
          track,
          persons)
    }

    private fun parsePerson(reader: JsonReader): Person {
      var id: Int? = null
      var name: String? = null
      reader.beginObject()
      while (reader.hasNext()) {
        when (reader.nextName()) {
          "id" -> id = reader.nextInt()
          "name" -> name = reader.nextString()
          "public_name" -> name = reader.nextString()
          else -> reader.skipValue()
        }
      }
      reader.endObject()
      return Person(
          id ?: throw IllegalStateException(),
          name ?: throw IllegalStateException())
    }

  }

}