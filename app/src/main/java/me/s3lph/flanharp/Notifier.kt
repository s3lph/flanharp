package me.s3lph.flanharp

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

class Notifier private constructor() {

  var manager: NotificationManager? = null
  private var idGenerator = 0

  @SuppressLint("NewApi")
  fun init(context: Context) {
    manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
    val channelReminder =
        NotificationChannel(CHANNEL_REMINDER,
            context.getString(R.string.nchannel_reminders),
            NotificationManager.IMPORTANCE_HIGH)
    channelReminder.enableLights(true)
    channelReminder.enableVibration(true)
    channelReminder.setShowBadge(true)
    manager?.createNotificationChannel(channelReminder)

    val channelBg =
        NotificationChannel(CHANNEL_BACKGROUND,
            context.getString(R.string.nchannel_background),
            NotificationManager.IMPORTANCE_LOW)
    channelBg.enableLights(false)
    channelBg.enableVibration(false)
    channelBg.setShowBadge(false)
    manager?.createNotificationChannel(channelBg)
  }

  fun generateId() = idGenerator++

  companion object {

    const val CHANNEL_BACKGROUND = "background"
    const val CHANNEL_REMINDER = "reminder"

    private var myInstance: Notifier? = null

    fun getInstance(): Notifier {
      if (myInstance == null) {
        myInstance = Notifier()
      }
      return myInstance as Notifier
    }

  }

}