package me.s3lph.flanharp.database

import java.io.Serializable

data class WebUrl(val weburlId: Long,
                  val conferenceId: Long,
                  val title: String?,
                  val url: String,
                  val cookie: String?) : Serializable