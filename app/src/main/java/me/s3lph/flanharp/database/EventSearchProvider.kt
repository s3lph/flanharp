package me.s3lph.flanharp.database

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import me.s3lph.flanharp.FHApplication

class EventSearchProvider: ContentProvider() {

  companion object {
    var confId: Long = -1L
  }

  override fun onCreate(): Boolean {
    return true
  }

  override fun getType(uri: Uri): String? {
    return null
  }

  override fun query(uri: Uri, projection: Array<String>?,
                     selection: String?,
                     selectionArgs: Array<String>?,
                     sortOrder: String?): Cursor? {
    val partialSearch = uri.lastPathSegment?.lowercase() ?: return null
    if (partialSearch.length < 3) {
      return null
    }
    return (FHApplication.getInstance())?.database?.searchEventSuggestions(confId, partialSearch)
  }

  override fun insert(uri: Uri, values: ContentValues?): Uri? {
    throw NotImplementedError()
  }

  override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
    throw NotImplementedError()
  }

  override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
    throw NotImplementedError()
  }

}