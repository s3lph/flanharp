package me.s3lph.flanharp.database

import android.app.SearchManager
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Color
import android.os.AsyncTask
import android.provider.BaseColumns
import android.util.Log
import me.s3lph.flanharp.fahrplan.*
import java.lang.ref.WeakReference
import java.sql.SQLException
import java.util.*
import kotlin.NoSuchElementException


fun <T> Array<T>.loop() = object : Iterable<T> {
  override fun iterator() = object : Iterator<T> {
    var i = -1
    override fun hasNext() = true
    override fun next(): T {
      i = (i + 1) % this@loop.count()
      return this@loop[i]
    }
  }
}

fun SQLiteDatabase.exclusiveTransaction(critical: (() -> Unit)) {
  try {
    this.beginTransaction()
    critical()
    this.setTransactionSuccessful()
  } finally {
    this.endTransaction()
  }
}

fun SQLiteDatabase.nonExclusiveTransaction(critical: (() -> Unit)) {
  try {
    this.beginTransactionNonExclusive()
    critical()
    this.setTransactionSuccessful()
  } finally {
    this.endTransaction()
  }
}

fun <O> async(block: () -> O, after: (O) -> Unit) {
  class Task : AsyncTask<Void, Void, O>() {
    override fun doInBackground(vararg input: Void): O {
      return block()
    }

    override fun onPostExecute(result: O) {
      after(result)
    }
  }
  Task().execute()
}

class FahrplanDatabase(context: WeakReference<Context>) :
    SQLiteOpenHelper(context.get(), DATABASE_NAME, null, DATABASE_VERSION) {

  companion object {
    const val DATABASE_VERSION = 8
    const val DATABASE_NAME = "fahrplan"

    const val TABLE_CONFERENCES = "conferences"
    const val TABLE_TRACKS = "tracks"
    const val TABLE_ROOMS = "rooms"
    const val TABLE_PEOPLE = "people"
    const val TABLE_EVENTS = "events"
    const val TABLE_PEOPLE_EVENTS = "people_events"
    const val TABLE_WEBURLS = "weburls"

    const val COLUMN_CONFERENCES_ID = "c_id"
    const val COLUMN_CONFERENCES_TITLE = "c_title"
    const val COLUMN_CONFERENCES_ACRONYM = "c_acronym"
    const val COLUMN_CONFERENCES_START = "c_start"
    const val COLUMN_CONFERENCES_END = "c_end"
    const val COLUMN_CONFERENCES_DAY_ANCHOR = "c_day_anchor"
    const val COLUMN_CONFERENCES_VERSION = "c_version"
    const val COLUMN_CONFERENCES_URL = "c_url"
    const val COLUMN_CONFERENCES_ANGELSHIFTS_URL = "c_shifts_url"

    const val COLUMN_WEBURLS_ID = "w_id"
    const val COLUMN_WEBURLS_TITLE = "w_title"
    const val COLUMN_WEBURLS_URL = "w_url"
    const val COLUMN_WEBURLS_CONFERENCE = "w_conference"
    const val COLUMN_WEBURLS_COOKIE = "w_cookie"

    const val COLUMN_TRACKS_ID = "t_id"
    const val COLUMN_TRACKS_NAME = "t_name"
    const val COLUMN_TRACKS_CONFERENCE = "t_conference"
    const val COLUMN_TRACKS_COLOR = "t_color"
    const val CONSTRAINT_TRACKS_UNIQUE_NAME = "t_name_conf_unique"

    const val COLUMN_ROOMS_ID = "r_id"
    const val COLUMN_ROOMS_CONFERENCE = "r_conference"
    const val COLUMN_ROOMS_NAME = "r_name"
    const val CONSTRAINT_ROOMS_UNIQUE_NAME = "r_name_conf_unique"

    const val COLUMN_PEOPLE_ID = "p_id"
    const val COLUMN_PEOPLE_CONFERENCE = "p_conference"
    const val COLUMN_PEOPLE_NAME = "p_name"

    const val COLUMN_EVENTS_ID = "e_id"
    const val COLUMN_EVENTS_CONFERENCE = "e_conference"
    const val COLUMN_EVENTS_TITLE = "e_title"
    const val COLUMN_EVENTS_SUBTITLE = "e_subtitle"
    const val COLUMN_EVENTS_ABSTRACT = "e_abstract"
    const val COLUMN_EVENTS_DESCRIPTION = "e_description"
    const val COLUMN_EVENTS_DAY = "e_day"
    const val COLUMN_EVENTS_START = "e_start"
    const val COLUMN_EVENTS_END = "e_end"
    const val COLUMN_EVENTS_TRACK = "e_track"
    const val COLUMN_EVENTS_TYPE = "e_type"
    const val COLUMN_EVENTS_ROOM = "e_room"
    const val COLUMN_EVENTS_URL = "e_url"
    const val COLUMN_EVENTS_FAV = "e_fav"
    const val COLUMN_EVENTS_SLUG = "e_slug"
    const val COLUMN_EVENTS_RECORDED = "e_recorded"

    const val COLUMN_PEOPLE_EVENTS_EVENT_ID = "pe_e_id"
    const val COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE = "pe_e_conference"
    const val COLUMN_PEOPLE_EVENTS_PERSON = "pe_p_id"
    const val COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE = "pe_p_conference"

  }

  override fun onCreate(database: SQLiteDatabase?) {
    database?.let { d ->
      try {
        d.exclusiveTransaction {
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_CONFERENCES (
                $COLUMN_CONFERENCES_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_CONFERENCES_TITLE TEXT UNIQUE NOT NULL,
                $COLUMN_CONFERENCES_ACRONYM TEXT DEFAULT NULL,
                $COLUMN_CONFERENCES_START INTEGER(8) NOT NULL DEFAULT 0,
                $COLUMN_CONFERENCES_END INTEGER(8) NOT NULL DEFAULT 0,
                $COLUMN_CONFERENCES_DAY_ANCHOR INTEGER(8) NOT NULL DEFAULT 0,
                $COLUMN_CONFERENCES_VERSION TEXT DEFAULT NULL,
                $COLUMN_CONFERENCES_URL TEXT NOT NULL,
                $COLUMN_CONFERENCES_ANGELSHIFTS_URL TEXT DEFAULT NULL
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_TRACKS (
                $COLUMN_TRACKS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_TRACKS_NAME TEXT NOT NULL,
                $COLUMN_TRACKS_CONFERENCE INTEGER NOT NULL,
                $COLUMN_TRACKS_COLOR TEXT NOT NULL DEFAULT '00dd00',
                CONSTRAINT $CONSTRAINT_TRACKS_UNIQUE_NAME
                  UNIQUE ($COLUMN_TRACKS_NAME, $COLUMN_TRACKS_CONFERENCE)
                  ON CONFLICT ROLLBACK,
                FOREIGN KEY ($COLUMN_TRACKS_CONFERENCE)
                  REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_ROOMS (
                $COLUMN_ROOMS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_ROOMS_NAME TEXT NOT NULL,
                $COLUMN_ROOMS_CONFERENCE INTEGER NOT NULL,
                CONSTRAINT $CONSTRAINT_ROOMS_UNIQUE_NAME
                  UNIQUE ($COLUMN_ROOMS_NAME, $COLUMN_ROOMS_CONFERENCE)
                  ON CONFLICT ROLLBACK,
                FOREIGN KEY ($COLUMN_ROOMS_CONFERENCE)
                  REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_PEOPLE (
                $COLUMN_PEOPLE_ID INTEGER NOT NULL,
                $COLUMN_PEOPLE_CONFERENCE INTEGER NOT NULL,
                $COLUMN_PEOPLE_NAME TEXT NOT NULL,
                PRIMARY KEY ($COLUMN_PEOPLE_ID, $COLUMN_PEOPLE_CONFERENCE),
                FOREIGN KEY ($COLUMN_PEOPLE_CONFERENCE)
                  REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_EVENTS (
                $COLUMN_EVENTS_ID INTEGER NOT NULL,
                $COLUMN_EVENTS_TITLE TEXT NOT NULL,
                $COLUMN_EVENTS_SUBTITLE TEXT DEFAULT NULL,
                $COLUMN_EVENTS_ABSTRACT TEXT DEFAULT NULL,
                $COLUMN_EVENTS_DESCRIPTION TEXT DEFAULT NULL,
                $COLUMN_EVENTS_CONFERENCE INTEGER NOT NULL,
                $COLUMN_EVENTS_TRACK INTEGER DEFAULT NULL,
                $COLUMN_EVENTS_DAY INTEGER NOT NULL DEFAULT 0,
                $COLUMN_EVENTS_START INTEGER(8) DEFAULT 0,
                $COLUMN_EVENTS_END INTEGER(8) DEFAULT 0,
                $COLUMN_EVENTS_ROOM INTEGER NOT NULL,
                $COLUMN_EVENTS_TYPE TEXT DEFAULT NULL,
                $COLUMN_EVENTS_URL TEXT DEFAULT NULL,
                $COLUMN_EVENTS_FAV INTEGER(1) NOT NULL DEFAULT 0,
                $COLUMN_EVENTS_RECORDED INTEGER(1) NOT NULL DEFAULT 0,
                $COLUMN_EVENTS_SLUG TEXT DEFAULT NULL,
                PRIMARY KEY ($COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE),
                FOREIGN KEY ($COLUMN_EVENTS_CONFERENCE)
                  REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY ($COLUMN_EVENTS_ROOM)
                  REFERENCES $TABLE_ROOMS($COLUMN_ROOMS_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY ($COLUMN_EVENTS_TRACK)
                  REFERENCES $TABLE_TRACKS($COLUMN_TRACKS_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_PEOPLE_EVENTS (
                $COLUMN_PEOPLE_EVENTS_EVENT_ID INTEGER NOT NULL,
                $COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE INTEGER NOT NULL,
                $COLUMN_PEOPLE_EVENTS_PERSON INTEGER NOT NULL,
                $COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE INTEGER NOT NULL,
                PRIMARY KEY ($COLUMN_PEOPLE_EVENTS_EVENT_ID,
                  $COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE,
                  $COLUMN_PEOPLE_EVENTS_PERSON,
                  $COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE),
                FOREIGN KEY ($COLUMN_PEOPLE_EVENTS_EVENT_ID, $COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE)
                  REFERENCES $TABLE_EVENTS($COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE)
                  ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY ($COLUMN_PEOPLE_EVENTS_PERSON, $COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE)
                  REFERENCES $TABLE_PEOPLE($COLUMN_PEOPLE_ID, $COLUMN_PEOPLE_CONFERENCE)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
          d.execSQL("""
              CREATE TABLE IF NOT EXISTS $TABLE_WEBURLS (
                $COLUMN_WEBURLS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_WEBURLS_CONFERENCE INTEGER NOT NULL,
                $COLUMN_WEBURLS_TITLE TEXT DEFAULT NULL,
                $COLUMN_WEBURLS_URL TEXT NOT NULL,
                $COLUMN_WEBURLS_COOKIE TEXT DEFAULT NULL,
                FOREIGN KEY ($COLUMN_WEBURLS_CONFERENCE)
                  REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
                  ON DELETE CASCADE ON UPDATE CASCADE
              )
              """.trimIndent())
        }
      } catch (e: SQLException) {
        e.printStackTrace()
      }
    }
  }

  override fun onUpgrade(database: SQLiteDatabase?, from: Int, to: Int) {
    if (from < 2) {
      // Add slug for media.ccc.de link generation
      database?.execSQL("""
        ALTER TABLE $TABLE_EVENTS
          ADD COLUMN $COLUMN_EVENTS_SLUG TEXT DEFAULT NULL
      """.trimIndent())
    }

    if (from < 3) {
      // Tracks and rooms tables had a wrong unique constraint each
      val tableUpgradeTracksTemp = "upgrade_tracks_temp"
      val tableUpgradeRoomsTemp = "upgrade_rooms_temp"

      database?.execSQL("""
          CREATE TEMPORARY TABLE $tableUpgradeRoomsTemp (
            $COLUMN_ROOMS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_ROOMS_NAME TEXT NOT NULL,
            $COLUMN_ROOMS_CONFERENCE INTEGER NOT NULL,
            CONSTRAINT $CONSTRAINT_ROOMS_UNIQUE_NAME
              UNIQUE ($COLUMN_ROOMS_NAME, $COLUMN_ROOMS_CONFERENCE)
              ON CONFLICT ROLLBACK,
            FOREIGN KEY ($COLUMN_ROOMS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $tableUpgradeRoomsTemp
            SELECT * FROM $TABLE_ROOMS
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $TABLE_ROOMS
      """.trimIndent())
      database?.execSQL("""
          CREATE TABLE $TABLE_ROOMS (
            $COLUMN_ROOMS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_ROOMS_NAME TEXT NOT NULL,
            $COLUMN_ROOMS_CONFERENCE INTEGER NOT NULL,
            CONSTRAINT $CONSTRAINT_ROOMS_UNIQUE_NAME
              UNIQUE ($COLUMN_ROOMS_NAME, $COLUMN_ROOMS_CONFERENCE)
              ON CONFLICT ROLLBACK,
            FOREIGN KEY ($COLUMN_ROOMS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $TABLE_ROOMS
            SELECT * FROM $tableUpgradeRoomsTemp
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $tableUpgradeRoomsTemp
      """.trimIndent())

      database?.execSQL("""
          CREATE TEMPORARY TABLE $tableUpgradeTracksTemp (
            $COLUMN_TRACKS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_TRACKS_NAME TEXT NOT NULL,
            $COLUMN_TRACKS_CONFERENCE INTEGER NOT NULL,
            $COLUMN_TRACKS_COLOR TEXT NOT NULL DEFAULT '00dd00',
            CONSTRAINT $CONSTRAINT_TRACKS_UNIQUE_NAME
              UNIQUE ($COLUMN_TRACKS_NAME, $COLUMN_TRACKS_CONFERENCE)
              ON CONFLICT ROLLBACK,
            FOREIGN KEY ($COLUMN_TRACKS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $tableUpgradeTracksTemp
            SELECT * FROM $TABLE_TRACKS
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $TABLE_TRACKS
      """.trimIndent())
      database?.execSQL("""
          CREATE TABLE $TABLE_TRACKS (
            $COLUMN_TRACKS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
            $COLUMN_TRACKS_NAME TEXT NOT NULL,
            $COLUMN_TRACKS_CONFERENCE INTEGER NOT NULL,
            $COLUMN_TRACKS_COLOR TEXT NOT NULL DEFAULT '00dd00',
            CONSTRAINT $CONSTRAINT_TRACKS_UNIQUE_NAME
              UNIQUE ($COLUMN_TRACKS_NAME, $COLUMN_TRACKS_CONFERENCE)
              ON CONFLICT ROLLBACK,
            FOREIGN KEY ($COLUMN_TRACKS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $TABLE_TRACKS
            SELECT * FROM $tableUpgradeTracksTemp
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $tableUpgradeTracksTemp
      """.trimIndent())
    }

    if (from < 4) {
      val tableUpgradeEventsTemp = "upgrade_events_temp"

      database?.execSQL("""
          CREATE TEMPORARY TABLE $tableUpgradeEventsTemp (
            $COLUMN_EVENTS_ID INTEGER NOT NULL,
            $COLUMN_EVENTS_TITLE TEXT NOT NULL,
            $COLUMN_EVENTS_SUBTITLE TEXT DEFAULT NULL,
            $COLUMN_EVENTS_ABSTRACT TEXT DEFAULT NULL,
            $COLUMN_EVENTS_DESCRIPTION TEXT DEFAULT NULL,
            $COLUMN_EVENTS_CONFERENCE INTEGER NOT NULL,
            $COLUMN_EVENTS_TRACK INTEGER DEFAULT NULL,
            $COLUMN_EVENTS_START INTEGER(8) DEFAULT 0,
            $COLUMN_EVENTS_END INTEGER(8) DEFAULT 0,
            $COLUMN_EVENTS_ROOM INTEGER NOT NULL,
            $COLUMN_EVENTS_TYPE TEXT DEFAULT NULL,
            $COLUMN_EVENTS_URL TEXT DEFAULT NULL,
            $COLUMN_EVENTS_FAV INTEGER(1) NOT NULL DEFAULT 0,
            $COLUMN_EVENTS_SLUG TEXT DEFAULT NULL,
            PRIMARY KEY ($COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE),
            FOREIGN KEY ($COLUMN_EVENTS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE,
            FOREIGN KEY ($COLUMN_EVENTS_ROOM)
              REFERENCES $TABLE_ROOMS($COLUMN_ROOMS_ID)
              ON DELETE CASCADE ON UPDATE CASCADE,
            FOREIGN KEY ($COLUMN_EVENTS_TRACK)
              REFERENCES $TABLE_TRACKS($COLUMN_TRACKS_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $tableUpgradeEventsTemp
            SELECT * FROM $TABLE_EVENTS
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $TABLE_EVENTS
      """.trimIndent())
      database?.execSQL("""
          CREATE TABLE $TABLE_EVENTS (
            $COLUMN_EVENTS_ID INTEGER NOT NULL,
            $COLUMN_EVENTS_TITLE TEXT NOT NULL,
            $COLUMN_EVENTS_SUBTITLE TEXT DEFAULT NULL,
            $COLUMN_EVENTS_ABSTRACT TEXT DEFAULT NULL,
            $COLUMN_EVENTS_DESCRIPTION TEXT DEFAULT NULL,
            $COLUMN_EVENTS_CONFERENCE INTEGER NOT NULL,
            $COLUMN_EVENTS_TRACK INTEGER DEFAULT NULL,
            $COLUMN_EVENTS_START INTEGER(8) DEFAULT 0,
            $COLUMN_EVENTS_END INTEGER(8) DEFAULT 0,
            $COLUMN_EVENTS_ROOM INTEGER NOT NULL,
            $COLUMN_EVENTS_TYPE TEXT DEFAULT NULL,
            $COLUMN_EVENTS_URL TEXT DEFAULT NULL,
            $COLUMN_EVENTS_FAV INTEGER(1) NOT NULL DEFAULT 0,
            $COLUMN_EVENTS_SLUG TEXT DEFAULT NULL,
            PRIMARY KEY ($COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE),
            FOREIGN KEY ($COLUMN_EVENTS_CONFERENCE)
              REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
              ON DELETE CASCADE ON UPDATE CASCADE,
            FOREIGN KEY ($COLUMN_EVENTS_ROOM)
              REFERENCES $TABLE_ROOMS($COLUMN_ROOMS_ID)
              ON DELETE CASCADE ON UPDATE CASCADE,
            FOREIGN KEY ($COLUMN_EVENTS_TRACK)
              REFERENCES $TABLE_TRACKS($COLUMN_TRACKS_ID)
              ON DELETE CASCADE ON UPDATE CASCADE
          )
      """.trimIndent())
      database?.execSQL("""
          INSERT INTO $TABLE_EVENTS
            SELECT * FROM $tableUpgradeEventsTemp
      """.trimIndent())
      database?.execSQL("""
          DROP TABLE $tableUpgradeEventsTemp
      """.trimIndent())
    }

    if (from < 5) {
      database?.execSQL("""
          ALTER TABLE $TABLE_EVENTS
            ADD COLUMN $COLUMN_EVENTS_DAY INTEGER NOT NULL DEFAULT 0
      """.trimIndent())
    }

    if (from < 6) {
      // Changed data source from XML to JSON... attempt to fix the data sources by replacing the
      // .xml in the URL's with .json
      database?.execSQL("""
        UPDATE $TABLE_CONFERENCES
        SET $COLUMN_CONFERENCES_URL = REPLACE($COLUMN_CONFERENCES_URL, '.xml', '.json')
      """.trimIndent())
    }

    if (from < 7) {
      // Added event recording flag
      database?.execSQL("""
        ALTER TABLE $TABLE_EVENTS
        ADD COLUMN $COLUMN_EVENTS_RECORDED INTEGER(1) NOT NULL DEFAULT 0
      """.trimIndent())
    }

    if (from < 8) {
      database?.execSQL("""
        CREATE TABLE IF NOT EXISTS $TABLE_WEBURLS (
          $COLUMN_WEBURLS_ID INTEGER PRIMARY KEY AUTOINCREMENT,
          $COLUMN_WEBURLS_CONFERENCE INTEGER NOT NULL,
          $COLUMN_WEBURLS_TITLE TEXT DEFAULT NULL,
          $COLUMN_WEBURLS_URL TEXT NOT NULL,
          $COLUMN_WEBURLS_COOKIE TEXT DEFAULT NULL,
          FOREIGN KEY ($COLUMN_WEBURLS_CONFERENCE)
            REFERENCES $TABLE_CONFERENCES($COLUMN_CONFERENCES_ID)
            ON DELETE CASCADE ON UPDATE CASCADE
        )
        """.trimIndent())
    }
  }

  override fun onOpen(database: SQLiteDatabase?) {
    super.onOpen(database)
    database?.setForeignKeyConstraintsEnabled(true)
  }

  fun insertOrReplaceFahrplan(fahrplan: Fahrplan, angelshifts: Angelshifts? = null) {
    writableDatabase.let { d ->
      try {
        d.exclusiveTransaction {
          d.delete(TABLE_CONFERENCES,
              "$COLUMN_CONFERENCES_TITLE = ?",
              arrayOf(fahrplan.conference.title))

          val confValues = ContentValues()
          confValues.put(COLUMN_CONFERENCES_TITLE, fahrplan.conference.title)
          confValues.put(COLUMN_CONFERENCES_ACRONYM, fahrplan.conference.acronym)
          confValues.put(COLUMN_CONFERENCES_START, fahrplan.conference.start.time)
          confValues.put(COLUMN_CONFERENCES_END, fahrplan.conference.end.time)
          confValues.put(COLUMN_CONFERENCES_DAY_ANCHOR, fahrplan.dayAnchor.time)
          confValues.put(COLUMN_CONFERENCES_VERSION, fahrplan.version)
          confValues.put(COLUMN_CONFERENCES_URL, fahrplan.source)
          confValues.put(COLUMN_CONFERENCES_ANGELSHIFTS_URL, angelshifts?.source)
          val confId = d.insert(TABLE_CONFERENCES, null, confValues)

          val trackIds = mutableMapOf<String, Long>()
          fahrplan.tracks?.zip(Track.colors.loop())?.forEach { (it, color) ->
            val trackValues = ContentValues()
            trackValues.put(COLUMN_TRACKS_CONFERENCE, confId)
            trackValues.put(COLUMN_TRACKS_NAME, it.name)
            trackValues.put(COLUMN_TRACKS_COLOR, color)
            val trackId = d.insert(TABLE_TRACKS, null, trackValues)
            trackIds[it.name] = trackId
          }
          val trackValues = ContentValues()
          trackValues.put(COLUMN_TRACKS_CONFERENCE, confId)
          trackValues.put(COLUMN_TRACKS_NAME, Angelshifts.TYPESTRING)
          trackValues.put(COLUMN_TRACKS_COLOR, Color.DKGRAY)
          val trackId = d.insert(TABLE_TRACKS, null, trackValues)
          trackIds[Angelshifts.TYPESTRING] = trackId

          val roomIds = mutableMapOf<String, Long>()
          fahrplan.rooms?.forEach {
            val roomValues = ContentValues()
            roomValues.put(COLUMN_ROOMS_CONFERENCE, confId)
            roomValues.put(COLUMN_ROOMS_NAME, it.name)
            val roomId = d.insert(TABLE_ROOMS, null, roomValues)
            roomIds[it.name] = roomId
          }
          val roomValues = ContentValues()
          roomValues.put(COLUMN_ROOMS_CONFERENCE, confId)
          roomValues.put(COLUMN_ROOMS_NAME, Angelshifts.TYPESTRING)
          val roomId = d.insert(TABLE_ROOMS, null, roomValues)
          roomIds[Angelshifts.TYPESTRING] = roomId

          fahrplan.people.forEach {
            val peopleValues = ContentValues()
            peopleValues.put(COLUMN_PEOPLE_CONFERENCE, confId)
            peopleValues.put(COLUMN_PEOPLE_ID, it.id)
            peopleValues.put(COLUMN_PEOPLE_NAME, it.name)
            d.insert(TABLE_PEOPLE, null, peopleValues)
          }

          fahrplan.events.forEach { event ->
            val eventValues = ContentValues()
            eventValues.put(COLUMN_EVENTS_CONFERENCE, confId)
            eventValues.put(COLUMN_EVENTS_ID, event.id)
            eventValues.put(COLUMN_EVENTS_TITLE, event.title)
            eventValues.put(COLUMN_EVENTS_SUBTITLE, event.subtitle)
            eventValues.put(COLUMN_EVENTS_ABSTRACT, event.abstract)
            eventValues.put(COLUMN_EVENTS_DESCRIPTION, event.description)
            eventValues.put(COLUMN_EVENTS_TRACK, trackIds[event.track])
            eventValues.put(COLUMN_EVENTS_ROOM, roomIds[event.room])
            eventValues.put(COLUMN_EVENTS_DAY, event.day)
            eventValues.put(COLUMN_EVENTS_START, event.start.time)
            eventValues.put(COLUMN_EVENTS_END, event.end.time)
            eventValues.put(COLUMN_EVENTS_TYPE, event.type)
            eventValues.put(COLUMN_EVENTS_URL, event.url.toString())
            eventValues.put(COLUMN_EVENTS_RECORDED, event.recorded)
            eventValues.put(COLUMN_EVENTS_SLUG, event.slug)
            d.insert(TABLE_EVENTS, null, eventValues)
            event.people.forEach { person ->
              val peopleEventValues = ContentValues()
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_EVENT_ID, event.id)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE, confId)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_PERSON, person.id)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE, confId)
              d.insert(TABLE_PEOPLE_EVENTS, null, peopleEventValues)
            }
          }

          angelshifts?.shifts?.forEach { shift ->
            val eventValues = ContentValues()
            eventValues.put(COLUMN_EVENTS_CONFERENCE, confId)
            eventValues.put(COLUMN_EVENTS_ID, shift.id)
            eventValues.put(COLUMN_EVENTS_TITLE, shift.title)
            eventValues.put(COLUMN_EVENTS_ABSTRACT, shift.room)
            eventValues.put(COLUMN_EVENTS_DESCRIPTION, shift.description)
            eventValues.put(COLUMN_EVENTS_TRACK, trackIds[Angelshifts.TYPESTRING])
            eventValues.put(COLUMN_EVENTS_ROOM, roomIds[Angelshifts.TYPESTRING])
            eventValues.put(COLUMN_EVENTS_DAY, shift.day)
            eventValues.put(COLUMN_EVENTS_START, shift.start.time)
            eventValues.put(COLUMN_EVENTS_END, shift.end.time)
            eventValues.put(COLUMN_EVENTS_TYPE, shift.type)
            eventValues.put(COLUMN_EVENTS_URL, shift.url.toString())
            d.insert(TABLE_EVENTS, null, eventValues)
          }
        }
      } catch (e: SQLException) {
        e.printStackTrace()
      }
    }
  }

  fun updateFahrplan(fahrplan: Fahrplan, angelshifts: Angelshifts? = null): List<String> {
    val favMap = mutableMapOf<Int, String>()
    writableDatabase.let { d ->
      try {
        d.exclusiveTransaction {

          val confId = d.query(TABLE_CONFERENCES,
              arrayOf(COLUMN_CONFERENCES_ID),
              "$COLUMN_CONFERENCES_TITLE = ?", arrayOf(fahrplan.conference.title),
              null, null, null, null).use idQuery@{
            if (it.moveToFirst()) {
              val id = it.getLong(0)
              it.close()
              return@idQuery id
            }
            it.close()
            throw NoSuchElementException("The conference ${fahrplan.conference.title} doesn't exist")
          }

          var angelshiftTrackId: Long
          var angelshiftRoomId: Long
          d.query(TABLE_EVENTS, arrayOf(COLUMN_EVENTS_TITLE, COLUMN_EVENTS_ID),
              "$COLUMN_EVENTS_CONFERENCE = ? AND $COLUMN_EVENTS_FAV = 1", arrayOf(confId.toString()),
              null, null, null, null)?.use { c ->
            if (c.moveToFirst()) {
              do {
                favMap[c.getInt(1)] = c.getString(0)
              } while (c.moveToNext())
            }
          }

          val confValues = ContentValues()
          confValues.put(COLUMN_CONFERENCES_VERSION, fahrplan.version)
          d.update(TABLE_CONFERENCES, confValues,
              "$COLUMN_CONFERENCES_ID = ?", arrayOf(confId.toString()))

          d.delete(TABLE_TRACKS, "$COLUMN_TRACKS_CONFERENCE = ?", arrayOf(confId.toString()))
          d.delete(TABLE_ROOMS, "$COLUMN_ROOMS_CONFERENCE = ?", arrayOf(confId.toString()))
          d.delete(TABLE_PEOPLE, "$COLUMN_PEOPLE_CONFERENCE = ?", arrayOf(confId.toString()))
          d.delete(TABLE_EVENTS, "$COLUMN_EVENTS_CONFERENCE = ?", arrayOf(confId.toString()))
          d.delete(TABLE_PEOPLE_EVENTS, "$COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE= ?", arrayOf(confId.toString()))

          val trackIds = mutableMapOf<String, Long>()
          fahrplan.tracks?.zip(Track.colors.loop())?.forEach { (it, color) ->
            val trackValues = ContentValues()
            trackValues.put(COLUMN_TRACKS_CONFERENCE, confId)
            trackValues.put(COLUMN_TRACKS_NAME, it.name)
            trackValues.put(COLUMN_TRACKS_COLOR, color)
            val trackId = d.insert(TABLE_TRACKS, null, trackValues)
            trackIds[it.name] = trackId
          }
          val trackValues = ContentValues()
          trackValues.put(COLUMN_TRACKS_CONFERENCE, confId)
          trackValues.put(COLUMN_TRACKS_NAME, Angelshifts.TYPESTRING)
          trackValues.put(COLUMN_TRACKS_COLOR, Color.DKGRAY)
          angelshiftTrackId = d.insert(TABLE_TRACKS, null, trackValues)
          trackIds[Angelshifts.TYPESTRING] = angelshiftTrackId

          val roomIds = mutableMapOf<String, Long>()
          fahrplan.rooms?.forEach {
            val roomValues = ContentValues()
            roomValues.put(COLUMN_ROOMS_CONFERENCE, confId)
            roomValues.put(COLUMN_ROOMS_NAME, it.name)
            val roomId = d.insert(TABLE_ROOMS, null, roomValues)
            roomIds[it.name] = roomId
          }
          val roomValues = ContentValues()
          roomValues.put(COLUMN_ROOMS_CONFERENCE, confId)
          roomValues.put(COLUMN_ROOMS_NAME, Angelshifts.TYPESTRING)
          angelshiftRoomId = d.insert(TABLE_ROOMS, null, roomValues)
          roomIds[Angelshifts.TYPESTRING] = angelshiftRoomId

          fahrplan.people.forEach {
            val peopleValues = ContentValues()
            peopleValues.put(COLUMN_PEOPLE_CONFERENCE, confId)
            peopleValues.put(COLUMN_PEOPLE_ID, it.id)
            peopleValues.put(COLUMN_PEOPLE_NAME, it.name)
            d.insert(TABLE_PEOPLE, null, peopleValues)
          }

          fahrplan.events.forEach { event ->
            val eventValues = ContentValues()
            eventValues.put(COLUMN_EVENTS_CONFERENCE, confId)
            eventValues.put(COLUMN_EVENTS_ID, event.id)
            eventValues.put(COLUMN_EVENTS_TITLE, event.title)
            eventValues.put(COLUMN_EVENTS_SUBTITLE, event.subtitle)
            eventValues.put(COLUMN_EVENTS_ABSTRACT, event.abstract)
            eventValues.put(COLUMN_EVENTS_DESCRIPTION, event.description)
            eventValues.put(COLUMN_EVENTS_TRACK, trackIds[event.track])
            eventValues.put(COLUMN_EVENTS_ROOM, roomIds[event.room])
            eventValues.put(COLUMN_EVENTS_DAY, event.day)
            eventValues.put(COLUMN_EVENTS_START, event.start.time)
            eventValues.put(COLUMN_EVENTS_END, event.end.time)
            eventValues.put(COLUMN_EVENTS_TYPE, event.type)
            if (favMap.containsKey(event.id)) {
              eventValues.put(COLUMN_EVENTS_FAV, 1)
              favMap.remove(event.id)
            }
            eventValues.put(COLUMN_EVENTS_URL, event.url.toString())
            eventValues.put(COLUMN_EVENTS_RECORDED, event.recorded)
            eventValues.put(COLUMN_EVENTS_SLUG, event.slug)
            d.insert(TABLE_EVENTS, null, eventValues)
            event.people.forEach { person ->
              val peopleEventValues = ContentValues()
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_EVENT_ID, event.id)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE, confId)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_PERSON, person.id)
              peopleEventValues.put(COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE, confId)
              d.insert(TABLE_PEOPLE_EVENTS, null, peopleEventValues)
            }
          }

          //Always update angelshifts
          angelshifts?.shifts?.forEach { shift ->
            val eventValues = ContentValues()
            eventValues.put(COLUMN_EVENTS_CONFERENCE, confId)
            eventValues.put(COLUMN_EVENTS_ID, shift.id)
            eventValues.put(COLUMN_EVENTS_TITLE, shift.title)
            eventValues.put(COLUMN_EVENTS_ABSTRACT, shift.room)
            eventValues.put(COLUMN_EVENTS_DESCRIPTION, shift.description)
            eventValues.put(COLUMN_EVENTS_TRACK, angelshiftTrackId)
            eventValues.put(COLUMN_EVENTS_ROOM, angelshiftRoomId)
            eventValues.put(COLUMN_EVENTS_DAY, shift.day)
            eventValues.put(COLUMN_EVENTS_START, shift.start.time)
            eventValues.put(COLUMN_EVENTS_END, shift.end.time)
            eventValues.put(COLUMN_EVENTS_TYPE, shift.type)
            eventValues.put(COLUMN_EVENTS_URL, shift.url.toString())
            d.insert(TABLE_EVENTS, null, eventValues)
          }

        }
      } catch (e: SQLException) {
        e.printStackTrace()
      }
    }
    return favMap.values.toList()
  }

  private fun fetchConferences(): List<Conference> {
    val confs = mutableListOf<Conference>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.query(TABLE_CONFERENCES,
            arrayOf(COLUMN_CONFERENCES_ID,
                COLUMN_CONFERENCES_TITLE,
                COLUMN_CONFERENCES_ACRONYM,
                COLUMN_CONFERENCES_START,
                COLUMN_CONFERENCES_END,
                COLUMN_CONFERENCES_DAY_ANCHOR,
                COLUMN_CONFERENCES_URL,
                COLUMN_CONFERENCES_ANGELSHIFTS_URL,
                COLUMN_CONFERENCES_VERSION),
            null, null, null, null, null)
        if (c.moveToFirst()) {
          do {
            val id = c.getLong(0)
            val title = c.getString(1)
            val acronym = c.getString(2)
            val start = Date(c.getLong(3))
            val end = Date(c.getLong(4))
            val dayAnchor = Date(c.getLong(5))
            val url = c.getString(6)
            val angelshiftsUrl = c.getString(7)
            val version = c.getString(8)
            confs.add(Conference(title, acronym, start, end, dayAnchor, url, angelshiftsUrl, version, id))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return confs
  }

  fun fetchConferencesAsync(done: (List<Conference>) -> Unit) {
    async({ fetchConferences() }, done)
  }

  private fun fetchDays(conference: Conference): List<DisplayDay> {
    val days = mutableMapOf<Int, DisplayDay>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.query(TABLE_EVENTS, arrayOf(COLUMN_EVENTS_START, COLUMN_EVENTS_END),
            "$COLUMN_EVENTS_CONFERENCE = ?", arrayOf(conference.id.toString()),
            null, null, null, null)
        if (c.moveToFirst()) {
          do {
            val start = Date(c.getLong(0))
            val end = Date(c.getLong(1))
            val startDay = ((start.time - conference.dayAnchor.time) / 86400_000).toInt() + 1
            val endDay = ((end.time - conference.dayAnchor.time) / 86400_000).toInt() + 1
            if (!days.containsKey(startDay)) {
              days[startDay] = DisplayDay(startDay, start, end)
            }
            if (!days.containsKey(endDay)) {
              days[endDay] = DisplayDay(endDay, start, end)
            }
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return days.values.toList()
  }

  fun fetchDaysAsync(conference: Conference, done: (List<DisplayDay>) -> Unit) {
    async({ fetchDays(conference) }, done)
  }

  private fun fetchUpcomingEventIds(conference: Conference): List<EventID> {
    val now = Date().time.toString()
    val upcoming = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            WHERE $COLUMN_EVENTS_CONFERENCE = ?
                AND $COLUMN_EVENTS_START >= ?
                AND $COLUMN_EVENTS_START < ? + 1800000
            """.trimIndent(),
            arrayOf(conference.id.toString(), now, now))
        if (c.moveToFirst()) {
          do {
            upcoming.add(EventID(c.getLong(0), c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return upcoming
  }

  fun fetchUpcomingEventIdsAsync(conference: Conference, done: (List<EventID>) -> Unit) {
    async({ fetchUpcomingEventIds(conference) }, done)
  }

  private fun fetchFavoriteIds(conference: Conference? = null, includeAngelshifts: Boolean = true): List<EventID> {
    val favs = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            LEFT JOIN $TABLE_TRACKS
              ON $COLUMN_EVENTS_TRACK = $COLUMN_TRACKS_ID
            ${if (conference != null) "WHERE $COLUMN_EVENTS_CONFERENCE = ?" else "WHERE ?"}
              AND ($COLUMN_EVENTS_FAV = 1 ${if (includeAngelshifts) "OR $COLUMN_TRACKS_NAME = '${Angelshifts.TYPESTRING}'" else ""})
            """.trimIndent(),
            arrayOf(conference?.id?.toString() ?: "1"))
        if (c.moveToFirst()) {
          do {
            favs.add(EventID(c.getLong(0), c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return favs
  }

  fun fetchFavoriteIdsAsync(conference: Conference? = null, includeAngelshifts: Boolean = true, done: (List<EventID>) -> Unit) {
    async({ fetchFavoriteIds(conference, includeAngelshifts) }, done)
  }

  private fun fetchUnrecordedEventIds(conference: Conference): List<EventID> {
    val unrecorded = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            WHERE $COLUMN_EVENTS_CONFERENCE = ?
                AND $COLUMN_EVENTS_RECORDED = 0
            """.trimIndent(),
                arrayOf(conference.id.toString()))
        if (c.moveToFirst()) {
          do {
            unrecorded.add(EventID(c.getLong(0), c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return unrecorded
  }

  fun fetchUnrecordedEventIdsAsync(conference: Conference, done: (List<EventID>) -> Unit) {
    async({ fetchUnrecordedEventIds(conference) }, done)
  }

  private fun fetchAngelshiftIds(conference: Conference): List<EventID> {
    val shifts = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            JOIN $TABLE_TRACKS
            ON $COLUMN_EVENTS_TRACK = $COLUMN_TRACKS_ID
            WHERE $COLUMN_EVENTS_CONFERENCE = ?
              AND $COLUMN_TRACKS_NAME = '${Angelshifts.TYPESTRING}'
            """.trimIndent(),
            arrayOf(conference.id.toString()))
        if (c.moveToFirst()) {
          do {
            shifts.add(EventID(c.getLong(0), c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return shifts
  }

  fun fetchAngelshiftIdsAsync(conference: Conference, done: (List<EventID>) -> Unit) {
    async({ fetchAngelshiftIds(conference) }, done)
  }

  private fun fetchEventsById(ids: List<EventID>): Map<EventID, DisplayEvent> {
    val events = mutableMapOf<EventID, DisplayEvent>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE, $COLUMN_EVENTS_TITLE,
              $COLUMN_EVENTS_SUBTITLE, $COLUMN_EVENTS_ABSTRACT, $COLUMN_EVENTS_DESCRIPTION,
              $COLUMN_EVENTS_START, $COLUMN_EVENTS_END, $COLUMN_TRACKS_NAME,
              $COLUMN_TRACKS_COLOR, $COLUMN_ROOMS_NAME, $COLUMN_EVENTS_TYPE, $COLUMN_EVENTS_URL,
              $COLUMN_EVENTS_FAV, $COLUMN_EVENTS_RECORDED, $COLUMN_EVENTS_SLUG,
              $COLUMN_CONFERENCES_TITLE, $COLUMN_CONFERENCES_ACRONYM,
              COALESCE(GROUP_CONCAT($COLUMN_PEOPLE_NAME), '')
            FROM $TABLE_EVENTS
            JOIN $TABLE_CONFERENCES ON $COLUMN_EVENTS_CONFERENCE = $COLUMN_CONFERENCES_ID
            LEFT JOIN $TABLE_TRACKS ON $COLUMN_EVENTS_TRACK = $COLUMN_TRACKS_ID
            JOIN $TABLE_ROOMS ON $COLUMN_EVENTS_ROOM = $COLUMN_ROOMS_ID
            LEFT JOIN $TABLE_PEOPLE_EVENTS
              ON $COLUMN_EVENTS_ID = $COLUMN_PEOPLE_EVENTS_EVENT_ID
              AND $COLUMN_EVENTS_CONFERENCE = $COLUMN_PEOPLE_EVENTS_EVENT_CONFERENCE
            LEFT JOIN $TABLE_PEOPLE
              ON $COLUMN_PEOPLE_EVENTS_PERSON = $COLUMN_PEOPLE_ID
              AND $COLUMN_PEOPLE_EVENTS_PERSON_CONFERENCE = $COLUMN_PEOPLE_CONFERENCE
            WHERE ${ids.fold("0") { xs, x ->
          "$xs OR ($COLUMN_EVENTS_ID = ${x.eventId} AND $COLUMN_EVENTS_CONFERENCE = ${x.conferenceId})"
        }}
            GROUP BY $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            ORDER BY $COLUMN_EVENTS_START ASC
            """.trimIndent(),
            arrayOf())
        if (c.moveToFirst()) {
          do {
            val room = Room(c.getString(10))
            val eventID = EventID(c.getLong(0), c.getLong(1))
            events[eventID] = DisplayEvent(
                eventID,
                c.getString(2),  // title
                c.getString(3),  // subtitle
                c.getString(4),  // abstract
                c.getString(5),  // description
                Date(c.getLong(6)),  // start
                Date(c.getLong(7)),  // end
                c.getString(8),   // track
                room.color,      // color
                room.name,  // room
                c.getString(11),  // type
                c.getString(12),  // url
                c.getInt(14) > 0,  // recorded
                c.getString(15),  // slug
                c.getString(16),  // conference title
                c.getString(17),  // conference acronym
                c.getString(18).replace(Regex(",(?!\\s)"), ", "),  //people
                c.getInt(13) != 0  // fav
            )
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return events
  }

  fun fetchEventsByIdAsync(ids: List<EventID>, done: (Map<EventID, DisplayEvent>) -> Unit) {
    async({ fetchEventsById(ids) }, done)
  }

  private fun fetchEventsByDay(confId: Long, day: Int): List<EventID> {
    val events = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        var c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            WHERE $COLUMN_EVENTS_CONFERENCE = ? AND $COLUMN_EVENTS_DAY = ?
            """.trimIndent(),
            arrayOf(confId.toString(), day.toString())
        )
        if (c.moveToFirst()) {
          do {
            events.add(
                EventID(
                    c.getLong(0),
                    c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
        // Fetch angelshifts separately
        c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            JOIN $TABLE_CONFERENCES
              ON $COLUMN_EVENTS_CONFERENCE = $COLUMN_CONFERENCES_ID
            JOIN $TABLE_TRACKS
              ON $COLUMN_EVENTS_TRACK = $COLUMN_TRACKS_ID
            WHERE $COLUMN_EVENTS_CONFERENCE = ?
              AND $COLUMN_TRACKS_NAME = '${Angelshifts.TYPESTRING}'
              AND $COLUMN_EVENTS_END >= $COLUMN_CONFERENCES_DAY_ANCHOR + 86400000 * (?-1)
              AND $COLUMN_EVENTS_START < $COLUMN_CONFERENCES_DAY_ANCHOR + 86400000 * (?)
            """.trimIndent(),
            arrayOf(confId.toString(), day.toString(), day.toString())
        )
        if (c.moveToFirst()) {
          do {
            events.add(
                EventID(
                    c.getLong(0),
                    c.getLong(1)))
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return events
  }

  fun fetchEventsByDayAsync(confId: Long, day: Int, done: (List<EventID>) -> Unit) {
    async({ fetchEventsByDay(confId, day) }, done)
  }


  private fun searchEvents(confId: Long, query: String): List<EventID> {
    val events = mutableListOf<EventID>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        // This is really horrible performance-wise, but sqlite3 FTS is weird...
        val c = d.rawQuery("""
            SELECT $COLUMN_EVENTS_ID, $COLUMN_EVENTS_CONFERENCE
            FROM $TABLE_EVENTS
            WHERE $COLUMN_EVENTS_CONFERENCE = ?
              AND ($COLUMN_EVENTS_TITLE LIKE '%' || ? || '%' COLLATE NOCASE
                OR $COLUMN_EVENTS_ABSTRACT LIKE '%' || ? || '%' COLLATE NOCASE
                OR $COLUMN_EVENTS_DESCRIPTION LIKE '%' || ? || '%' COLLATE NOCASE)
            """.trimIndent(),
            arrayOf(confId.toString(), query, query))
        Log.e("FD:search", "Started search")
        if (c.moveToFirst()) {
          do {
            events.add(
                EventID(
                    c.getLong(0),
                    c.getLong(1)))
            Log.e("FD:search", "Found: ${events.last()}")
          } while (c.moveToNext())
        }
        c.close()
      }
    }
    return events
  }

  fun searchEventsAsync(confId: Long, query: String, done: (List<EventID>) -> Unit) {
    async({ searchEvents(confId, query) }, done)
  }

  fun searchEventSuggestions(confId: Long, query: String): Cursor? {
    return readableDatabase?.rawQuery("""
        SELECT
          $COLUMN_EVENTS_ID AS ${BaseColumns._ID},
          $COLUMN_EVENTS_TITLE AS ${SearchManager.SUGGEST_COLUMN_TEXT_1},
          $COLUMN_EVENTS_START || ' @ ' || $COLUMN_ROOMS_NAME AS ${SearchManager.SUGGEST_COLUMN_TEXT_2},
          $COLUMN_EVENTS_ID AS ${SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID}
        FROM $TABLE_EVENTS
        JOIN $TABLE_ROOMS
          ON $COLUMN_EVENTS_ROOM = $COLUMN_ROOMS_ID
        WHERE $COLUMN_EVENTS_CONFERENCE = ?
          AND ($COLUMN_EVENTS_TITLE LIKE '%' || ? || '%' COLLATE NOCASE
            OR $COLUMN_EVENTS_ABSTRACT LIKE '%' || ? || '%' COLLATE NOCASE
            OR $COLUMN_EVENTS_DESCRIPTION LIKE '%' || ? || '%' COLLATE NOCASE)
    """.trimIndent(),
        arrayOf(confId.toString(), query, query, query))
  }

  private fun toggleFav(event: DisplayEvent): Boolean {
    val turnOn = !event.reminder
    writableDatabase?.let { d ->
      try {
        d.exclusiveTransaction {
          val values = ContentValues()
          values.put(COLUMN_EVENTS_FAV, if (turnOn) 1 else 0)
          d.update(TABLE_EVENTS,
              values,
              "$COLUMN_EVENTS_CONFERENCE = ? AND $COLUMN_EVENTS_ID = ?",
              arrayOf(event.id.conferenceId.toString(), event.id.eventId.toString()))
        }
      } catch (e: SQLException) {
        e.printStackTrace()
        return !turnOn
      }
    }
    return turnOn
  }

  fun toggleFavAsync(event: DisplayEvent, done: (Boolean) -> Unit) {
    async({ toggleFav(event) }, done)
  }

  private fun deleteConference(confId: Long): List<Conference> {
    writableDatabase?.let { d ->
      try {
        d.exclusiveTransaction {
          d.delete(TABLE_CONFERENCES,
              "$COLUMN_CONFERENCES_ID = ?",
              arrayOf(confId.toString()))
        }
        return fetchConferences()
      } catch (e: SQLException) {
        e.printStackTrace()
      }
    }
    return emptyList()
  }

  fun deleteConferenceAsync(confId: Long, done: (List<Conference>) -> Unit) {
    async({ deleteConference(confId) }, done)
  }

  private fun fetchConferenceUrls(confId: Long): Pair<String?, String?>? {
    var urls: Pair<String?, String?>? = null
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.query(TABLE_CONFERENCES,
            arrayOf(COLUMN_CONFERENCES_URL, COLUMN_CONFERENCES_ANGELSHIFTS_URL),
            "$COLUMN_CONFERENCES_ID = ?", arrayOf(confId.toString()),
            null, null, null, null)
        if (c.moveToFirst()) {
          val fahrplanUrl = c.getString(0)
          val angelshiftsUrl = c.getString(1)
          urls = Pair(fahrplanUrl, angelshiftsUrl)
          c.close()
        }
      }
    }
    return urls
  }

  fun fetchConferenceUrlsAsync(confId: Long, done: (Pair<String?, String?>?) -> Unit) {
    async({ fetchConferenceUrls(confId) }, done)
  }

  private fun updateConferenceUrls(confId: Long, fahrplanUrl: String, angelshiftsUrl: String) {
    writableDatabase?.let { d ->
      try {
        d.exclusiveTransaction {
          val cv = ContentValues()
          cv.put(COLUMN_CONFERENCES_URL, fahrplanUrl)
          cv.put(COLUMN_CONFERENCES_ANGELSHIFTS_URL, if (angelshiftsUrl.isEmpty()) null else angelshiftsUrl)
          d.update(TABLE_CONFERENCES, cv,
              "$COLUMN_CONFERENCES_ID = ?", arrayOf(confId.toString()))
        }
      } catch (e: SQLException) {
        e.printStackTrace()
      }
    }
  }

  fun updateConferenceUrlsAsync(confId: Long, fahrplanUrl: String, angelshiftsUrl: String, done: (Unit) -> Unit) {
    async({ updateConferenceUrls(confId, fahrplanUrl, angelshiftsUrl) }, done)
  }

  private fun fetchWebrls(confId: Long): List<WebUrl> {
    val urls = mutableListOf<WebUrl>()
    readableDatabase?.let { d ->
      d.nonExclusiveTransaction {
        val c = d.query(TABLE_WEBURLS,
                arrayOf(COLUMN_WEBURLS_ID, COLUMN_WEBURLS_CONFERENCE, COLUMN_WEBURLS_TITLE, COLUMN_WEBURLS_URL, COLUMN_WEBURLS_COOKIE),
                "$COLUMN_WEBURLS_CONFERENCE = ?", arrayOf(confId.toString()),
                null, null, null, null)
        if (!c.moveToFirst()) {
          c.close()
          return@nonExclusiveTransaction
        }
        do {
          val weburlId = c.getLong(0)
          val weburlConference = c.getLong(1)
          val weburlTitle = c.getString(2)
          val weburlUrl = c.getString(3)
          val weburlCookie = c.getString(4)
          urls.add(WebUrl(weburlId, weburlConference, weburlTitle, weburlUrl, weburlCookie))
        } while (c.moveToNext())
        c.close()
      }
    }
    return urls
  }

  fun fetchWebUrlsAsync(confId: Long, done: (List<WebUrl>) -> Unit) {
    async({ fetchWebrls(confId) }, done)
  }

  private fun updateCookie(weburlId: Long, cookie: String?) {
    if (cookie == null) {
      return
    }
    writableDatabase.let { d ->
      val values = ContentValues()
      values.put(COLUMN_WEBURLS_COOKIE, cookie)
      d.update(TABLE_WEBURLS, values,
              "$COLUMN_WEBURLS_ID = ?", arrayOf(weburlId.toString()))
    }
  }

  fun updateCookieAsync(weburlId: Long, cookie: String?, done: (Unit) -> Unit) {
    async({ updateCookie(weburlId, cookie) }, done)
  }

  fun addWeburl(conference: Long, title: String?, url: String): WebUrl {
    writableDatabase.let { d ->
      val values = ContentValues()
      values.put(COLUMN_WEBURLS_CONFERENCE, conference)
      values.put(COLUMN_WEBURLS_TITLE, title)
      values.put(COLUMN_WEBURLS_URL, url)
      val rowid = d.insert(TABLE_WEBURLS, null, values)
      return WebUrl(rowid, conference, title, url, null)
    }
  }

  fun addWeburlAsync(conference: Long, title: String?, url: String, done: (WebUrl) -> Unit) {
    async({ addWeburl(conference, title, url) }, done)
  }

}
