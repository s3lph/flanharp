package me.s3lph.flanharp.database

import java.io.Serializable

data class EventID(val eventId: Long, val conferenceId: Long) : Serializable