package me.s3lph.flanharp.reminder

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.Notifier
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.ui.EventDetailActivity
import java.text.SimpleDateFormat
import java.util.*

class ReminderReceiver : BroadcastReceiver() {

  private fun notify(context: Context, title: String, time: Date, place: String, id: EventID, header: String?, color: Int) {
    Notifier.getInstance().let { notifier ->
      val notificationId = notifier.generateId()
      val builder = NotificationCompat.Builder(context, Notifier.CHANNEL_REMINDER)
      val tapIntent = Intent(context.applicationContext, EventDetailActivity::class.java)
      tapIntent.putExtra("event_id", id)
      tapIntent.putExtra("notification_id", notificationId)
      builder.setContentTitle(title)
          .setSmallIcon(R.mipmap.ic_launcher)
          .setContentText(String.format("%s @ %s",
              SimpleDateFormat.getTimeInstance().format(time),
              place))
          .setShowWhen(true)
          .setColor(color)
          .setDefaults(NotificationCompat.DEFAULT_ALL)
          .setContentIntent(PendingIntent.getActivity(
              context.applicationContext,
              id.hashCode(),
              tapIntent,
              PendingIntent.FLAG_CANCEL_CURRENT))
      header?.let { builder.setSubText(it) }
      notifier.manager?.notify(notificationId, builder.build())
    }
  }

  override fun onReceive(context: Context?, intent: Intent?) {
    when (intent?.action) {
      Intent.ACTION_BOOT_COMPLETED -> {
        //no-op; alarm registration done in FHApplication
      }
      "reminder" -> {
        val title = intent.getStringExtra("event_title") as String
        val start = intent.getSerializableExtra("event_start") as Date
        val room = intent.getStringExtra("event_room") as String
        val eId = intent.getLongExtra("event_id", -1)
        val cId = intent.getLongExtra("conference_id", -1)
        val eventId = EventID(eId, cId)
        val color = intent.getIntExtra("event_color", -1)
        val conferenceTitle = intent.getStringExtra("conference_title")
        context?.let {
          notify(it, title, start, room, eventId, conferenceTitle, color)
        }
      }
      "download_cancel" -> {
        intent.extras?.let { extras ->
          (extras["task_id"] as Int?)?.let {
            FHApplication.getInstance()?.cancelTask(it)
          }
        }
      }
    }
  }
}
