package me.s3lph.flanharp.ui

import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.fahrplan.DisplayEvent
import java.util.*

class DayCalendarActivity : AppCompatActivity() {

  private lateinit var calendarRoot: RelativeLayout
  private lateinit var calendarHeader: RelativeLayout

  private val roomMap = mutableMapOf<String, Int>()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_day_calendar)
    calendarRoot = findViewById(R.id.day_calendar_wrapper)
    calendarHeader = findViewById(R.id.day_calendar_header)
    title = intent?.extras?.get("title") as String?
    supportActionBar?.subtitle = intent?.extras?.get("subtitle") as String?
  }

  override fun onResume() {
    super.onResume()
    intent?.extras?.let { e ->
      @Suppress("UNCHECKED_CAST")
      (e.get("events") as List<EventID>?)?.let { ids ->
        (application as FHApplication).database.fetchEventsByIdAsync(ids) {
          setupCalendarUiNew(it.values.toList())
        }
      } ?: run {
        (e.get("conf_id") as Long?)?.let { confId ->
          (e.get("day") as Int?)?.let { day ->
            (application as FHApplication).database.fetchEventsByDayAsync(confId, day) { events ->
              (application as FHApplication).database.fetchEventsByIdAsync(events) {
                setupCalendarUiNew(it.values.toList())
              }
            }
          }
        }
      }
    }
  }

  private fun setupCalendarUi(events: List<DisplayEvent>, h: Int, start: Date, end: Date, minOffset: Int) {
    roomMap.clear()
    if (events.isEmpty()) {
      return
    }

    findViewById<View>(R.id.morebg)?.let { bgview ->
      val now = Date()
      bgview.background = createActiveBackground(h, start, end, now)
    }
    events.asSequence().map { it.room }.filter { it != null }.distinct().toList().zip(generateSequence(0) { it + 1 }.asIterable()).forEach {
      roomMap[it.first!!] = it.second
    }
    val width = (resources.displayMetrics.widthPixels - scale(25)) / 4
    for ((room, i) in roomMap.entries) {
      val title = TextView(this)
      title.text = room
      title.maxLines = 3
      title.ellipsize = TextUtils.TruncateAt.END
      title.setPadding(10, 10, 10, 10)
      calendarHeader.addView(title)
      (title.layoutParams as RelativeLayout.LayoutParams?)?.let { params ->
        params.leftMargin = scale(25) + i * width
        params.width = width
      }
    }
    events.forEach { event ->
      layoutInflater.inflate(R.layout.day_calendar_item, calendarRoot, false)?.let { view ->
        calendarRoot.addView(view)
        view.setBackgroundColor(event.color)
        view.findViewById<TextView>(R.id.day_calendar_item_title)?.text = event.title
        view.findViewById<TextView>(R.id.day_calendar_item_subtitle)?.text = event.subtitle
        view.findViewById<ImageView>(R.id.day_calendar_item_fav)?.visibility =
            if (event.reminder) View.VISIBLE else View.INVISIBLE
        view.findViewById<TextView>(R.id.day_calendar_item_recorded)?.visibility =
            if (event.recorded) View.VISIBLE else View.INVISIBLE
        (view.layoutParams as RelativeLayout.LayoutParams?)?.let { params ->
          params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
          params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
          val top = ((event.start.time - start.time) * h / (end.time - start.time)).toInt()
          val bottom = ((event.end.time - start.time) * h / (end.time - start.time)).toInt()
          params.topMargin = top
          params.width = width
          params.height = bottom - top
          params.leftMargin = scale(25) + (roomMap[event.room] ?: 0) * width
          view.layoutParams = params
        }
        view.setOnClickListener {
          val intent = Intent(this, EventDetailActivity::class.java)
          intent.putExtra("event", event)
          startActivity(intent)
        }
      }
    }
    calendarHeader.requestLayout()
    calendarRoot.requestLayout()
  }

  private fun createActiveBackground(h: Int, start: Date, end: Date, middle: Date): Drawable {
    val bitmap = Bitmap.createBitmap(1, h, Bitmap.Config.ARGB_8888)
    val now = ((middle.time - start.time) * h / (end.time - start.time)).toInt()
    if (now < 0 || now >= h) {
      for (i in 0 until h) {
        bitmap.setPixel(0, i, getColor(android.R.color.transparent))
      }
      return BitmapDrawable(resources, bitmap)
    }
   for (i in 0 until now) {
      bitmap.setPixel(0, i, getColor(R.color.greyed_out))
    }
    for (i in now until h) {
      bitmap.setPixel(0, i, getColor(android.R.color.transparent))
    }
    bitmap.setPixel(0, now, getColor(R.color.now))
    return BitmapDrawable(resources, bitmap)
  }

  private fun scale(x: Int) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, x.toFloat(), resources.displayMetrics).toInt()


  private fun setupCalendarUiNew(events: List<DisplayEvent>) {
    roomMap.clear()
    if (events.isEmpty()) {
      return
    }

    val sCal = Calendar.getInstance()
    sCal.time = events.map { it.start }.min()
    sCal.add(Calendar.MINUTE, -30)

    val eCal = Calendar.getInstance()
    eCal.time = events.map { it.end }.max()
    eCal.add(Calendar.MINUTE, 30)

    val hours = ((eCal.timeInMillis - sCal.timeInMillis) / 3_600_000L).toInt()
    val h = scale(hours * 100)
    val minOffset = scale(-sCal.get(Calendar.MINUTE) * 100 / 60)

    val bmpH = scale(100)
    val bmpOffset = ((minOffset % 100) + 100) % 100
    val bitmap = Bitmap.createBitmap(1, bmpH, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    val paint = Paint()
    paint.color = 0xff444444.toInt()
    for (y in 0 until scale(2)) {
      canvas.drawPoint(0.0f, ((y + bmpOffset) % bmpH).toFloat(), paint)
    }
    for (y in 0 until scale(1)) {
      canvas.drawPoint(0.0f, ((scale(25) + y + bmpOffset) % bmpH).toFloat(), paint)
      canvas.drawPoint(0.0f, ((scale(50) + y + bmpOffset) % bmpH).toFloat(), paint)
      canvas.drawPoint(0.0f, ((scale(75) + y + bmpOffset) % bmpH).toFloat(), paint)
    }
    val newDrawable = BitmapDrawable(resources, bitmap)
    newDrawable.tileModeX = Shader.TileMode.REPEAT
    newDrawable.tileModeY = Shader.TileMode.REPEAT
    calendarRoot.background = newDrawable

    calendarRoot.layoutParams.height = h

    val zeroHour = sCal.get(Calendar.HOUR_OF_DAY)
    for (y in 1 until hours) {
      TextView(this).let { textView ->
        calendarRoot.addView(textView)
        textView.text = String.format("%02dh", (zeroHour + y) % 24)
        textView.textSize = 12.5f
        (textView.layoutParams as RelativeLayout.LayoutParams?)?.let { params ->
          params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
          params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
          params.topMargin = scale(y * 100 - 15) + minOffset
        }
      }
    }

    setupCalendarUi(events, h, sCal.time, eCal.time, minOffset)
  }


}
