package me.s3lph.flanharp.ui.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.DisplayEvent
import java.text.SimpleDateFormat
import java.util.*

class EventListAdapter(private val context: Context) : BaseAdapter() {

  private var events: List<DisplayEvent> = listOf()

  private val inflater: LayoutInflater by lazy {
    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
  }
  private val formatter = SimpleDateFormat.getDateTimeInstance()

  private fun makeView(parent: ViewGroup?): View? {
    return inflater.inflate(R.layout.event_list_item, parent, false)
  }

  private fun createActiveBackground(start: Date, end: Date, middle: Date): Drawable {
    val bitmap = Bitmap.createBitmap(1, 100, Bitmap.Config.ARGB_8888)
    val x = ((middle.time - start.time) * 100.0 / (end.time - start.time)).toInt()
    for (i in 0 until x) {
      bitmap.setPixel(0, i, context.getColor(R.color.greyed_out))
    }
    for (i in x until 100) {
      bitmap.setPixel(0, i, context.getColor(android.R.color.transparent))
    }
    bitmap.setPixel(0, x, context.getColor(R.color.now))
    return BitmapDrawable(context.resources, bitmap)
  }

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    (convertView ?: makeView(parent))?.let { view ->
      val now = Date()
      view.background = when {
        events[position].end < now -> context.getDrawable(R.color.greyed_out)
        events[position].start > now -> context.getDrawable(android.R.color.transparent)
        else -> createActiveBackground(events[position].start, events[position].end, now)
      }
      view.findViewById<TextView>(R.id.event_list_item_title)?.text = events[position].title
      view.findViewById<TextView>(R.id.event_list_item_subtitle)?.let {
        val subtitle = events[position].subtitle
        if (subtitle == null || subtitle.isEmpty()) {
          it.visibility = View.GONE
        } else {
          it.visibility = View.VISIBLE
          it.text = subtitle
        }
      }
      view.findViewById<TextView>(R.id.event_list_item_duration)?.text =
          String.format("%s — %s @ %s %s",
              formatter.format(events[position].start),
              formatter.format(events[position].end),
              if (events[position].track == Angelshifts.TYPESTRING)
                events[position].abstract else events[position].room,
              if (events[position].recorded) "\uD83D\uDCF9" else "")
      view.findViewById<TextView>(R.id.event_list_item_persons)?.let {
        val people = events[position].people
        if (people.isEmpty()) {
          it.visibility = View.GONE
        } else {
          it.visibility = View.VISIBLE
          it.text = people
        }
      }
      view.findViewById<View>(R.id.event_list_item_color)?.background = ColorDrawable(events[position].color)
      view.findViewById<View>(R.id.event_list_item_fav)?.visibility =
          if (events[position].reminder) View.VISIBLE else View.INVISIBLE
      return view
    }
    throw NullPointerException()
  }

  override fun getItem(position: Int): Any {
    return events[position]
  }

  override fun getItemId(position: Int): Long {
    return events[position].id.eventId
  }

  override fun getCount(): Int {
    return events.size
  }

  fun setDataSorce(data: List<DisplayEvent>) {
    events = data
  }
}