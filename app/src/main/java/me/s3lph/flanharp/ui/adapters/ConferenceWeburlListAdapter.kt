package me.s3lph.flanharp.ui.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.WebUrl
import me.s3lph.flanharp.ui.EventListActivity
import me.s3lph.flanharp.ui.WeburlActivity
import java.io.Serializable
import java.lang.ref.WeakReference

class ConferenceWeburlListAdapter(private val context: Context, private var weburls: List<WebUrl>) : BaseAdapter() {

  var dialog: WeakReference<DialogInterface>? = null

  private val inflater: LayoutInflater by lazy {
    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
  }

  private fun makeView(parent: ViewGroup?): View? {
    return inflater.inflate(android.R.layout.simple_list_item_2, parent, false)
  }

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    (convertView ?: makeView(parent))?.let {
      it.findViewById<TextView>(android.R.id.text1)?.text = weburls[position].title ?: weburls[position].url
      it.findViewById<TextView>(android.R.id.text2)?.text = weburls[position].url
      it.setOnClickListener { _ ->
        val intent = Intent(context, WeburlActivity::class.java)
        intent.putExtra("weburl_id", weburls[position].weburlId)
        intent.putExtra("weburl_title", weburls[position].title)
        intent.putExtra("weburl_url", weburls[position].url)
        intent.putExtra("weburl_cookie", weburls[position].cookie)
        dialog?.get()?.dismiss()
        context.startActivity(intent)
      }
      return it
    }
    throw NullPointerException()
  }

  override fun getItem(position: Int): Any {
    return weburls[position]
  }

  override fun getItemId(position: Int): Long {
    return position.toLong()
  }

  override fun getCount(): Int {
    return weburls.size
  }

  fun setDataSource(data: List<WebUrl>) {
    weburls = data
  }
}