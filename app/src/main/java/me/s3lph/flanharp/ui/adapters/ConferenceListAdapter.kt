package me.s3lph.flanharp.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import me.s3lph.flanharp.R
import me.s3lph.flanharp.fahrplan.Conference
import java.text.SimpleDateFormat

class ConferenceListAdapter(context: Context) : BaseAdapter() {

  private var conferences = listOf<Conference>()

  private val inflater: LayoutInflater by lazy {
    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
  }
  private val formatter = SimpleDateFormat.getDateInstance()

  private fun makeView(parent: ViewGroup?): View? {
    return inflater.inflate(R.layout.conference_list_item, parent, false)
  }

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    (convertView ?: makeView(parent))?.let {
      it.findViewById<TextView>(R.id.conference_list_item_title)?.text = conferences[position].title
      it.findViewById<TextView>(R.id.conference_list_item_duration)?.text =
          String.format("%s — %s",
              formatter.format(conferences[position].start),
              formatter.format(conferences[position].end))
      return it
    }
    throw NullPointerException()
  }

  override fun getItem(position: Int): Any {
    return conferences[position]
  }

  override fun getItemId(position: Int): Long {
    return conferences[position].id ?: -1L
  }

  override fun getCount(): Int {
    return conferences.size
  }

  fun setDataSource(data: List<Conference>) {
    conferences = data
  }
}