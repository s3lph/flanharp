package me.s3lph.flanharp.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventSearchProvider
import me.s3lph.flanharp.dataproc.CreateFahrplanTask
import me.s3lph.flanharp.dataproc.UpdateFahrplanTask
import me.s3lph.flanharp.fahrplan.Conference
import me.s3lph.flanharp.fahrplan.DisplayDay
import me.s3lph.flanharp.ui.adapters.ConferenceDaysListAdapter
import me.s3lph.flanharp.ui.adapters.ConferenceWeburlListAdapter
import java.io.Serializable
import java.lang.ref.WeakReference

class ConferenceDayListActivity : AppCompatActivity() {

  private val conference: Conference by lazy {
    intent?.extras?.get("conference") as Conference
  }
  private val adapter: ConferenceDaysListAdapter by lazy {
    ConferenceDaysListAdapter(applicationContext)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_conference_day_list)
    title = conference.title
    supportActionBar?.subtitle = getString(R.string.subtitle_version, conference.version)

    val dayList = findViewById<ListView>(android.R.id.list)
    dayList.adapter = adapter
    dayList.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
      when (pos) {
        adapter.days.size -> showUpNext()
        adapter.days.size + 1 -> showFavorites()
        adapter.days.size + 2 -> showUnrecorded()
        adapter.days.size + 3 -> showAngelshifts()
        else -> showDay(adapter.getItem(pos) as DisplayDay)
      }
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    super.onCreateOptionsMenu(menu)
    menuInflater.inflate(R.menu.menu_conference_day_list, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.menu_conference_day_list_update -> {
        UpdateFahrplanTask(WeakReference(applicationContext)) {
          finish()
        }.execute(conference.url, conference.angelshiftsUrl)
        true
      }
      R.id.menu_conference_day_list_search -> {
        this.onSearchRequested()
        true
      }
      R.id.menu_conference_day_list_weburls -> {
        this.showWeburls()
        true
      }
      else -> false
    }
  }

  override fun onResume() {
    super.onResume()
    conference.id?.let { EventSearchProvider.confId = it }
    (application as FHApplication).let { app ->
      app.database.fetchDaysAsync(conference) { days ->
        app.database.fetchUpcomingEventIdsAsync(conference) { upcoming ->
          app.database.fetchUnrecordedEventIdsAsync(conference) { unrecorded ->
            app.database.fetchFavoriteIdsAsync(conference, includeAngelshifts = false) { favs ->
              app.database.fetchAngelshiftIdsAsync(conference) { shifts ->
                adapter.setDataSource(days, upcoming, favs, unrecorded, shifts)
                adapter.notifyDataSetChanged()
              }
            }
          }
        }
      }
    }
  }

  override fun startActivity(intent: Intent?) {
    // Add the conference ID to the intent when starting a search
    if (intent?.action == Intent.ACTION_SEARCH) {
      conference.id?.let { intent.putExtra("conf_id", it) }
    }
    super.startActivity(intent)
  }

  private fun showUpNext() {
    val intent = Intent(this, EventListActivity::class.java)
    intent.putExtra("events", adapter.upcomingEvents as Serializable)
    intent.putExtra("title", getString(R.string.up_now))
    intent.putExtra("subtitle", conference.title)
    startActivity(intent)
  }

  private fun showFavorites() {
    val intent = Intent(this, EventListActivity::class.java)
    intent.putExtra("events", adapter.favorites as Serializable)
    intent.putExtra("title", getString(R.string.favorites))
    intent.putExtra("subtitle", conference.title)
    startActivity(intent)
  }

  private fun showUnrecorded() {
    val intent = Intent(this, EventListActivity::class.java)
    intent.putExtra("events", adapter.unrecordedEvents as Serializable)
    intent.putExtra("title", getString(R.string.unrecorded))
    intent.putExtra("subtitle", conference.title)
    startActivity(intent)
  }

  private fun showAngelshifts() {
    val intent = Intent(this, EventListActivity::class.java)
    intent.putExtra("events", adapter.angelshifts as Serializable)
    intent.putExtra("title", getString(R.string.angelshifts))
    intent.putExtra("subtitle", conference.title)
    startActivity(intent)
  }

  private fun showDay(day: DisplayDay) {
    val intent = Intent(this, DayCalendarActivity::class.java)
    intent.putExtra("day", day.index)
    intent.putExtra("conf_id", conference.id)
    intent.putExtra("title", getString(R.string.day_x, day.index))
    intent.putExtra("subtitle", conference.title)
    startActivity(intent)
  }

  private fun showWeburls() {
    conference.id?.let { id ->
      (application as FHApplication).let { app ->
        app.database.fetchWebUrlsAsync(id) { urls ->
          val builder = AlertDialog.Builder(this)
          val list = ListView(this)
          val adapter =  ConferenceWeburlListAdapter(this, urls)
          list.adapter = adapter
          val dialog = builder
                  .setView(list)
                  .setTitle(R.string.menu_item_weburls)
                  .setPositiveButton(R.string.add) { it, _ ->
                    it.dismiss()
                    addWeburl()
                  }
                  .setNegativeButton(R.string.cancel) { it, _ -> it.dismiss() }
                  .create()
          adapter.dialog = WeakReference(dialog)
          dialog.show()
        }
      }
    }
  }

  private fun addWeburl() {
    conference.id?.let { id ->
      (application as FHApplication).let { app ->
        val builder = AlertDialog.Builder(this)
        builder.setView(R.layout.weburl_add_view)
                .setTitle(R.string.title_add_weburl)
                .setPositiveButton(R.string.add) { it, _ ->
                  it.dismiss()
                  (it as AlertDialog).findViewById<EditText>(R.id.weburl_add_view_title)?.text.let { title ->
                    it.findViewById<EditText>(R.id.weburl_add_view_url)?.text?.let { url ->
                      app.database.addWeburlAsync(id, title.toString(), url.toString()) {}
                    }
                  }
                }
                .setNeutralButton(R.string.cancel) { it, _ -> it.dismiss() }
                .show()
      }
    }
  }
}
