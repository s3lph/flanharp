package me.s3lph.flanharp.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ListView
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.R
import me.s3lph.flanharp.dataproc.CreateFahrplanTask
import me.s3lph.flanharp.dataproc.UpdateFahrplanTask
import me.s3lph.flanharp.fahrplan.Conference
import me.s3lph.flanharp.ui.adapters.ConferenceListAdapter
import java.lang.ref.WeakReference
import java.net.URI
import java.net.URLDecoder

class MainActivity : AppCompatActivity() {

  private val adapter: ConferenceListAdapter by lazy {
    ConferenceListAdapter(applicationContext)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    val confList = findViewById<ListView>(android.R.id.list)
    confList.adapter = adapter
    confList.onItemClickListener = AdapterView.OnItemClickListener { parent, _, pos, _ ->
      val intent = Intent(this, ConferenceDayListActivity::class.java)
      intent.putExtra("conference", parent.adapter?.getItem(pos) as Conference)
      this.startActivity(intent)
    }
    confList.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, _, _, rowid ->
      showFahrplanEditDialog(rowid)
      true
    }
  }

  override fun onStart() {
    super.onStart()
    if (intent?.action == Intent.ACTION_VIEW) {
      var conferenceUrl = intent?.dataString
      try {
        val url = Uri.parse(intent?.dataString)
        if (url.host == "ggt.gaa.st") {
          Log.d("MainActivity", "Host is ggt.gaa.st, extracting url")
          val query = "http://localhost/?" + (URLDecoder.decode(url.fragment, Charsets.UTF_8.toString()) ?: "")
          Log.d("MainActivity", "query: ${query}")
          val qurl = Uri.parse(query)
          Log.d("MainActivity", "qurl: ${qurl}")
          conferenceUrl = qurl.getQueryParameter("url") ?:  conferenceUrl
        }
      } catch (e: Exception) {
        // no-op
      }
      Log.d("MainActivity", "conferenceUrl: ${conferenceUrl}")
      CreateFahrplanTask(WeakReference(applicationContext)) {
        adapter.notifyDataSetChanged()
      }.execute(conferenceUrl)
      intent.action = null
    }
  }

  override fun onResume() {
    super.onResume()
    (application as FHApplication).database.fetchConferencesAsync {
      adapter.setDataSource(it)
      adapter.notifyDataSetChanged()
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.menu_main, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.menu_main_add -> showFarplanDialog()
      R.id.menu_main_info -> showAppInfoDialog()
      else -> return false
    }
    return true
  }

  private fun showFarplanDialog() {
    val builder = AlertDialog.Builder(this)
    builder.setView(R.layout.fahrplan_url_dialog_view)
        .setTitle(R.string.fahrplan_url_dialog_title)
        .setPositiveButton(R.string.add_fahrplan) { it, _ ->
          it.dismiss()
          (it as AlertDialog).findViewById<EditText>(R.id.fahrplan_url_dialog_input_fahrplan_url)?.text?.let { fpUrl ->
            it.findViewById<EditText>(R.id.fahrplan_url_dialog_input_angelshifts_url)?.text.let { esUrl ->
              val oesUrl = if (esUrl?.length ?: 0 > 0) {
                esUrl
              } else {
                null
              }
              CreateFahrplanTask(WeakReference(applicationContext)) {
                adapter.notifyDataSetChanged()
              }.execute(fpUrl.toString(), oesUrl?.toString())
            }
          }
        }
        .setNeutralButton(R.string.cancel) { it, _ -> it.dismiss() }
        .show()
  }

  private fun showFahrplanEditDialog(rowid: Long) {
    (application as FHApplication).database.fetchConferenceUrlsAsync(rowid) { urls ->
      if (urls == null) {
        return@fetchConferenceUrlsAsync
      }
      val view = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
          .inflate(R.layout.fahrplan_url_dialog_view, null)
      val fUrlView = view.findViewById<EditText>(R.id.fahrplan_url_dialog_input_fahrplan_url)
      val aUrlView = view.findViewById<EditText>(R.id.fahrplan_url_dialog_input_angelshifts_url)
      fUrlView?.setText(urls.first)
      aUrlView?.setText(urls.second)
      val builder = AlertDialog.Builder(this)
      builder.setView(view)
          .setTitle(R.string.edit_fahrplan_url)
          .setPositiveButton(R.string.menu_item_update) { it, _ ->
            val fUrl = fUrlView.text.toString()
            val aUrl = aUrlView.text.toString()
            (application as FHApplication).database.updateConferenceUrlsAsync(rowid, fUrl, aUrl) {
              UpdateFahrplanTask(WeakReference(applicationContext))
                  .execute(fUrl, if (aUrl.isEmpty()) null else aUrl)
            }
            it.dismiss()
          }
          .setNeutralButton(R.string.delete) { it, _ ->
            (application as FHApplication).database.deleteConferenceAsync(rowid) {
              adapter.setDataSource(it)
              adapter.notifyDataSetChanged()
            }
            it.dismiss()
          }
          .setNegativeButton(R.string.cancel) { it, _ -> it.dismiss() }
          .show()
    }
  }

  private fun showAppInfoDialog() {
    val builder = AlertDialog.Builder(this)
    builder
        .setView(R.layout.main_app_info)
        .setTitle(R.string.app_name)
        .setPositiveButton(R.string.ok) { it, _ -> it.dismiss() }
        .show()
  }

}
