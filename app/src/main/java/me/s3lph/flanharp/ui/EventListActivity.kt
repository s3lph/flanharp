package me.s3lph.flanharp.ui

import android.app.SearchManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.AdapterView
import android.widget.ListView
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.fahrplan.DisplayEvent
import me.s3lph.flanharp.ui.adapters.EventListAdapter

class EventListActivity : AppCompatActivity() {

  private val sectionTitle: String by lazy {
    intent?.extras?.get("title") as String
  }
  private val adapter: EventListAdapter by lazy {
    EventListAdapter(applicationContext)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_event_list)
    if (intent.action == Intent.ACTION_VIEW) {
      title = sectionTitle
      supportActionBar?.subtitle = intent?.extras?.get("subtitle") as String?
    } else {
      title = getText(R.string.search_result_title)
      supportActionBar?.subtitle = intent.getStringExtra(SearchManager.QUERY)
    }
    val confList = findViewById<ListView>(android.R.id.list)
    confList.adapter = adapter
    confList.onItemClickListener = AdapterView.OnItemClickListener { _, _, pos, _ ->
      val intent = Intent(this, EventDetailActivity::class.java)
      intent.putExtra("event", adapter.getItem(pos) as DisplayEvent)
      startActivity(intent)
    }
  }

  private fun showEvents(eIdList: List<EventID>) {
    (application as FHApplication).database.fetchEventsByIdAsync(eIdList) { events ->
      adapter.setDataSorce(events.values.toList())
      adapter.notifyDataSetChanged()
    }
  }

  override fun onResume() {
    super.onResume()
    intent?.extras?.let { e ->
      when {
        intent.action == "me.s3lph.flanharp.SeachSuggestion" -> {
          intent.data?.lastPathSegment?.toLong()?.let { eId ->
            // TODO
          }
        }
        intent.action == Intent.ACTION_SEARCH -> {
          val query = intent.getStringExtra(SearchManager.QUERY) ?: ""
          Log.e("ELA:search", "search string: $query")
          (e.get("conf_id") as Long?)?.let { confId ->
            Log.e("ELA:search", "Starting search for conference $confId")
            (application as FHApplication).database.searchEventsAsync(confId, query) {
              showEvents(it)
            }
          }
        }
        e.get("events") != null -> {
          @Suppress("UNCHECKED_CAST")
          (e.get("events") as List<EventID>?)?.let { it ->
            showEvents(it)
          }
        }
        else -> {
          (e.get("conf_id") as Long?)?.let { confId ->
            (e.get("day") as Int?)?.let { day ->
              (application as FHApplication).database.fetchEventsByDayAsync(confId, day) {
                showEvents(it)
              }
            }
          }
        }
      }
    }
  }
}
