package me.s3lph.flanharp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.WebUrl

class WeburlActivity : AppCompatActivity() {

    val webview: WebView? by lazy { findViewById<WebView>(R.id.webview) }
    val cookieManager: CookieManager? by lazy { CookieManager.getInstance() }
    var weburl_id: Long? = null
    var url: String? = null
    var cookie: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weburl)
        weburl_id = intent.extras?.getLong("weburl_id", -1L)
        if ((weburl_id?.compareTo(0) ?: 0) < 0) {
            weburl_id = null
        }
        title = intent.extras?.getString("weburl_title", "") ?: ""
        url = intent.extras?.getString("weburl_url", "") ?: ""
        cookie = intent.extras?.getString("weburl_cookie", null)
    }

    override fun onStart() {
        super.onStart()
        url?.let { url ->
            cookie?.let { cookie ->
                cookieManager?.setCookie(url, cookie)
            }
            webview?.webViewClient = WebViewClient()
            webview?.settings?.javaScriptEnabled = true
            webview?.loadUrl(url)
        }
    }

    override fun onPause() {
        super.onPause()
        weburl_id?.let { id ->
            url?.let { url ->
                cookie = cookieManager?.getCookie(url)
                cookie?.let { cookie ->
                    (application as FHApplication).database.updateCookieAsync(id, cookie) {}
                }
            }
        }
    }

}