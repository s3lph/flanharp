package me.s3lph.flanharp.ui

import android.content.Intent
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import me.s3lph.flanharp.FHApplication
import me.s3lph.flanharp.Notifier
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.fahrplan.Angelshifts
import me.s3lph.flanharp.fahrplan.DisplayEvent
import java.text.SimpleDateFormat

class EventDetailActivity : AppCompatActivity() {

  private var event: DisplayEvent? = null

  private val formatter: SimpleDateFormat by lazy {
    SimpleDateFormat.getDateTimeInstance() as SimpleDateFormat
  }
  private var favMenuItem: MenuItem? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_event_detail)
  }

  override fun onResume() {
    super.onResume()
    intent?.extras?.let { extras ->
      extras.getInt("notification_id").let { id ->
        Notifier.getInstance().manager?.cancel(id)
      }

      extras.get("event")?.let { e ->
        event = e as DisplayEvent
        showEvent()
      } ?: (intent?.extras?.getSerializable("event_id") as EventID?)?.let { id ->
        (application as FHApplication).database.fetchEventsByIdAsync(listOf(id)) {
          event = it.values.first()
          showEvent()
        }
      }
    }

  }

  private fun showEvent() {
    event?.let { event ->
      if (event.track.orEmpty() == Angelshifts.TYPESTRING) {
        showAngelshift()
        return
      }
      title = event.title
      supportActionBar?.subtitle = event.subtitle
      this.findViewById<WebView>(R.id.event_detail_description)?.let { webview ->
        webview.loadData("<div>${event.abstract ?: ""}</div><br/><div>${event.description ?: ""}</div>",
            "text/html",
            "utf-8")
        webview.settings.javaScriptEnabled = false
        webview.setBackgroundResource(android.R.color.transparent)
      }
      this.findViewById<TextView>(R.id.event_detail_persons)?.text = event.people
      this.findViewById<TextView>(R.id.event_detail_track)?.let { textview ->
        textview.text = when {
          event.type == null && event.track == null -> ""
          event.type == null -> event.track
          event.track == null -> event.type
          else -> getString(R.string.track_plus_type, event.track, event.type)
        }
      }
      this.findViewById<TextView>(R.id.event_detail_duration)?.text =
          String.format("%s — %s %s",
              formatter.format(event.start),
              formatter.format(event.end),
              if (event.recorded) "\uD83D\uDCF9" else "")
      this.findViewById<TextView>(R.id.event_detail_place)?.text = event.room
    }
  }

  private fun showAngelshift() {
    event?.let { event ->
      title = event.title
      supportActionBar?.subtitle = event.subtitle
      this.findViewById<WebView>(R.id.event_detail_description)?.let { webview ->
        webview.loadData(event.description.orEmpty(),
            "text/html",
            "utf-8")
        webview.settings.javaScriptEnabled = false
        webview.setBackgroundResource(android.R.color.transparent)
      }
      this.findViewById<TextView>(R.id.event_detail_persons)?.text = getString(R.string.you)
      this.findViewById<TextView>(R.id.event_detail_track)?.let { textview ->
        textview.text = if (event.type == null) event.track
        else getString(R.string.track_plus_type, getString(R.string.angelshift), event.type)
        textview.compoundDrawablesRelative.first()?.let { drawable ->
          drawable.setTintList(null)
          drawable.setColorFilter(event.color, PorterDuff.Mode.MULTIPLY)
        }
      }
      this.findViewById<TextView>(R.id.event_detail_duration)?.text =
          String.format("%s — %s",
              formatter.format(event.start),
              formatter.format(event.end))
      this.findViewById<TextView>(R.id.event_detail_place)?.text = event.abstract
    }

  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.menu_event_detail, menu)
    favMenuItem = menu?.findItem(R.id.menu_event_detail_favorites)
    if (event?.track.orEmpty() == Angelshifts.TYPESTRING) {
      menu?.removeItem(R.id.menu_event_detail_favorites)
    }
    if (event?.slug == null) {
      menu?.removeItem(R.id.menu_event_detail_media)
    }
    updateFavMenuItem()
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.menu_event_detail_favorites -> toggleFav()
      R.id.menu_event_detail_media -> openMediaCccDe()
      else -> return super.onOptionsItemSelected(item)
    }
    return true
  }

  private fun updateFavMenuItem() {
    event?.let { event ->
      favMenuItem?.icon = AppCompatResources.getDrawable(this,
          if (event.reminder) R.drawable.favorite_on
          else R.drawable.favorite_off)
    }
  }

  private fun toggleFav() {
    event?.let { event ->
      (application as FHApplication).database.toggleFavAsync(event) { reminder ->
        event.reminder = reminder
        (application as FHApplication).updateReminder(event)
        updateFavMenuItem()
      }
    }
  }

  private fun openMediaCccDe() {
    event?.slug?.let { slug ->
      val url = "https://media.ccc.de/v/$slug/"
      val i = Intent(Intent.ACTION_VIEW, Uri.parse(url))
      startActivity(i)
    }
  }

}
