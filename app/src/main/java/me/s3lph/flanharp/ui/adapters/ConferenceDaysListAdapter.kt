package me.s3lph.flanharp.ui.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import me.s3lph.flanharp.R
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.fahrplan.DisplayDay
import java.text.SimpleDateFormat
import java.util.*

class ConferenceDaysListAdapter(private val context: Context) : BaseAdapter() {

  internal var days = listOf<DisplayDay>()
  internal var upcomingEvents = listOf<EventID>()
  internal var favorites = listOf<EventID>()
  internal var unrecordedEvents = listOf<EventID>()
  internal var angelshifts: List<EventID>? = null

  private val inflater: LayoutInflater by lazy {
    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
  }
  private val formatter = SimpleDateFormat.getDateInstance()

  private fun makeView(resId: Int, parent: ViewGroup?): View? {
    return inflater.inflate(resId, parent, false)
  }

  private fun createActiveBackground(start: Date, end: Date, middle: Date): Drawable {
    val bitmap = Bitmap.createBitmap(1, 100, Bitmap.Config.ARGB_8888)
    val x = ((middle.time - start.time) * 100.0 / (end.time - start.time)).toInt()
    for (i in 0 until x) {
      bitmap.setPixel(0, i, context.getColor(R.color.greyed_out))
    }
    for (i in x until 100) {
      bitmap.setPixel(0, i, context.getColor(android.R.color.transparent))
    }
    bitmap.setPixel(0, x, context.getColor(R.color.now))
    return BitmapDrawable(context.resources, bitmap)
  }

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    val now = Date()
    (convertView ?: makeView(R.layout.conference_day_list_item, parent))?.let {
      val titleView = it.findViewById<TextView>(R.id.conference_day_list_item_title)
      val durationView = it.findViewById<TextView>(R.id.conference_day_list_item_duration)
      when {
        position < days.size -> {
          it.background = when {
            days[position].end < now -> AppCompatResources.getDrawable(context, R.color.greyed_out)
            days[position].start > now -> AppCompatResources.getDrawable(context, android.R.color.transparent)
            else -> createActiveBackground(days[position].start, days[position].end, now)
          }
          titleView?.text = context.getString(R.string.day_x, days[position].index)
          durationView?.text = formatter.format(days[position].start)
        }
        position == days.size -> {
          it.background = AppCompatResources.getDrawable(context, android.R.color.transparent)
          titleView?.text = context.getString(R.string.up_now)
          durationView?.text = context.getString(R.string.up_now_stats, upcomingEvents.size)
        }
        position == days.size + 1 -> {
          it.background = AppCompatResources.getDrawable(context, android.R.color.transparent)
          titleView?.text = context.getString(R.string.favorites)
          durationView?.text = context.getString(R.string.number_of_favs, favorites.size)
        }
        position == days.size + 2 -> {
          it.background = AppCompatResources.getDrawable(context, android.R.color.transparent)
          titleView?.text = context.getString(R.string.unrecorded)
          durationView?.text = context.getString(R.string.number_of_unrecorded, unrecordedEvents.size)
        }
        position == days.size + 3 -> {
          it.background = AppCompatResources.getDrawable(context, android.R.color.transparent)
          titleView?.text = context.getString(R.string.angelshifts)
          durationView?.text = context.getString(R.string.number_of_shifts, angelshifts?.size)
        }
      }
      return it
    }
    throw NullPointerException()
  }

  override fun getItem(position: Int): Any {
    return if (position < days.size) days[position] else Unit
  }

  override fun getItemId(position: Int): Long {
    return -1L
  }

  override fun getCount(): Int {
    return days.size + if (angelshifts == null) 3 else 4
  }

  fun setDataSource(days: List<DisplayDay>,
                    upcomingEvents: List<EventID>,
                    favorites: List<EventID>,
                    unrecordedEvents: List<EventID>,
                    angelshifts: List<EventID>? = null) {
    this.days = days
    this.upcomingEvents = upcomingEvents
    this.favorites = favorites
    this.unrecordedEvents = unrecordedEvents
    this.angelshifts = angelshifts
  }

}