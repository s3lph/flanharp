package me.s3lph.flanharp.fahrplan

import android.net.Uri
import java.util.*

data class Event(val id: Int,
                 val title: String,
                 val subtitle: String?,
                 val abstract: String?,
                 val description: String?,
                 val day: Int,
                 val start: Date,
                 val end: Date,
                 val type: String,
                 val room: String,
                 val url: Uri?,
                 val recorded: Boolean,
                 val slug: String?,
                 val track: String?,
                 val people: List<Person>)
