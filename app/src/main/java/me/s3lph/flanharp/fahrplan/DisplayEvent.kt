package me.s3lph.flanharp.fahrplan

import me.s3lph.flanharp.database.EventID
import java.io.Serializable
import java.util.*

data class DisplayEvent(var id: EventID,
                        var title: String,
                        var subtitle: String?,
                        var abstract: String?,
                        var description: String?,
                        var start: Date,
                        var end: Date,
                        var track: String?,
                        var color: Int,
                        var room: String?,
                        var type: String?,
                        var url: String?,
                        var recorded: Boolean,
                        var slug: String?,
                        val conferenceTitle: String,
                        val conferenceAcronym: String?,
                        var people: String,
                        var reminder: Boolean) : Serializable