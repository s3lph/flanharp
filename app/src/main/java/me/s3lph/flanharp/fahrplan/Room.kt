package me.s3lph.flanharp.fahrplan

import android.graphics.Color
import java.nio.charset.Charset
import java.security.MessageDigest

data class Room(val name: String) {

  val color: Int by lazy {
    // Use the last byte of the sha1 hash over the room name as hue
    val hue = (MessageDigest
        .getInstance("SHA-1")
        .digest(name.toByteArray(Charset.defaultCharset()))
        .last()
        .toInt() + 128)
        .toFloat()
    Color.HSVToColor(255, floatArrayOf(hue, 1.0f, 1.0f))
  }

}