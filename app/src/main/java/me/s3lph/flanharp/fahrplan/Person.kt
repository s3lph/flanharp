package me.s3lph.flanharp.fahrplan

data class Person(val id: Int,
                  val name: String) {

  companion object {
    val YOU = Person(-1, "You")
  }

}