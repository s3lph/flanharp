package me.s3lph.flanharp.fahrplan

import java.util.*

data class Fahrplan(val source: String,
                    val version: String,
                    val conference: Conference,
                    val dayAnchor: Date,
                    val events: List<Event>,
                    val people: List<Person>) {

  val rooms: List<Room>? by lazy {
    events.asSequence().map { it.room }.distinct().map { Room(it) }.toList()
  }

  val tracks: List<Track>? by lazy {
    events.asSequence().map { it.track }.filterNotNull().distinct().map { Track(it, 0xff00dd00.toInt()) }.toList()
  }

}