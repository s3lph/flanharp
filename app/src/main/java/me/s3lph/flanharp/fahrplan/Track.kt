package me.s3lph.flanharp.fahrplan

data class Track(val name: String, val color: Int) {

  companion object {

    val colors = arrayOf(
        0xff_00ff00.toInt(),
        0xff_dddd00.toInt(),
        0xff_4444dd.toInt(),
        0xff_ff00ff.toInt(),
        0xff_880000.toInt(),
        0xff_ff8800.toInt(),
        0xff_00ffff.toInt()
    )

  }

}
