package me.s3lph.flanharp.fahrplan

data class Angelshifts(val shifts: List<Event>, val source: String) {

  companion object {
    const val TYPESTRING = "__angelshift__"
  }

}