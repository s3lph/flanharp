package me.s3lph.flanharp.fahrplan

import java.util.*

data class DisplayDay(var index: Int, var start: Date, var end: Date)