package me.s3lph.flanharp.fahrplan

import java.io.Serializable
import java.util.*

data class Conference(val title: String,
                      val acronym: String?,
                      val start: Date,
                      val end: Date,
                      val dayAnchor: Date,
                      val url: String,
                      val angelshiftsUrl: String? = null,
                      val version: String? = null,
                      val id: Long? = null) : Serializable
