package me.s3lph.flanharp

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import me.s3lph.flanharp.database.EventID
import me.s3lph.flanharp.database.FahrplanDatabase
import me.s3lph.flanharp.dataproc.FahrplanTask
import me.s3lph.flanharp.fahrplan.DisplayEvent
import me.s3lph.flanharp.reminder.ReminderReceiver
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

class FHApplication : Application() {

  companion object {
    private var appInstance: FHApplication? = null
    fun getInstance() = appInstance
  }

  val database: FahrplanDatabase by lazy {
    FahrplanDatabase(WeakReference(applicationContext))
  }

  private val manager: AlarmManager by lazy {
    getSystemService(Context.ALARM_SERVICE) as AlarmManager
  }

  private var requestCodeProvider = AtomicInteger(0)

  private val pendingIntents = ConcurrentHashMap<Int, PendingIntent>()
  private val eventIdMap = ConcurrentHashMap<EventID, Int>()

  private val downloadTasks = ConcurrentHashMap<Int, WeakReference<FahrplanTask>>()

  override fun onCreate() {
    super.onCreate()
    FHApplication.appInstance = this
    Notifier.getInstance().init(applicationContext)

    for ((_, pi) in pendingIntents.entries) {
      pi.cancel()
    }
    pendingIntents.clear()
    database.fetchFavoriteIdsAsync { favIds ->
      database.fetchEventsByIdAsync(favIds) { events ->
        events.values.forEach {
          updateReminder(it)
        }
      }
    }
  }

  fun updateReminder(event: DisplayEvent) {
    if (event.reminder && event.start >= Date()) {
      val intent = Intent(applicationContext, ReminderReceiver::class.java)
      intent.action = "reminder"
      intent.putExtra("event_title", event.title)
      intent.putExtra("event_id", event.id.eventId)
      intent.putExtra("conference_id", event.id.conferenceId)
      intent.putExtra("event_start", event.start)
      intent.putExtra("event_room", event.room)
      intent.putExtra("event_color", event.color)
      intent.putExtra("conference_title", event.conferenceTitle)
      val cal = Calendar.getInstance()
      cal.time = event.start
      cal.add(Calendar.MINUTE, -15)
      val requestCode = eventIdMap[event.id] ?: requestCodeProvider.getAndIncrement()
      val pending = PendingIntent.getBroadcast(
          applicationContext, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT
      )
      eventIdMap[event.id] = requestCode
      pendingIntents[requestCode] = pending
      manager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pending)
    } else {
      eventIdMap[event.id]?.let { requestCode ->
        pendingIntents[requestCode]?.let { pi ->
          manager.cancel(pi)
          pi.cancel()
          pendingIntents.remove(requestCode)
          eventIdMap.remove(event.id)
        }
      }
    }
  }

  fun registerTask(task: FahrplanTask): Pair<Int, PendingIntent> {
    val requestCode = requestCodeProvider.getAndIncrement()
    val cancelIntent = Intent(applicationContext, ReminderReceiver::class.java)
    cancelIntent.action = "download_cancel"
    cancelIntent.putExtra("task_id", requestCode)
    val pending = PendingIntent.getBroadcast(
        applicationContext, requestCode, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    )
    downloadTasks[requestCode] = WeakReference(task)
    return Pair(requestCode, pending)
  }

  fun unregisterTask(taskId: Int) {
    downloadTasks.remove(taskId)
  }

  fun cancelTask(requestCode: Int) {
    downloadTasks[requestCode]?.get()?.let { task ->
      task.requestCancel()
      downloadTasks.remove(requestCode)
    }
  }

}