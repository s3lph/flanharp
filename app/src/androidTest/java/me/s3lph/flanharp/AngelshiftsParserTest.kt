package me.s3lph.flanharp

import android.support.test.runner.AndroidJUnit4
import me.s3lph.flanharp.dataproc.AngelshiftsJsonParser
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.io.ByteArrayInputStream
import java.nio.charset.Charset

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class AngelshiftsParserTest {

  private val angelshiftsJson = """
[
  {
    "shifttype_id": 38,
    "name": "Bottle Collection",
    "id": 6018,
    "SID": 2738,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "Bottle Collection",
    "start": 1514386800,
    "end": 1514394000,
    "RID": 37,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 106,
    "created_at_timestamp": 1514338178,
    "edited_by_user_id": 1033,
    "edited_at_timestamp": 1514385674,
    "Name": "Bottle Sorting",
    "from_frab": 0,
    "map_url": "",
    "description": "The bottle sorting is situated in Hall 4 just next to the BOC."
  },
  {
    "shifttype_id": 38,
    "name": "Bottle Collection",
    "id": 7657,
    "SID": 3257,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "Bottle Collection for Saal A",
    "start": 1514421000,
    "end": 1514426400,
    "RID": 37,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 106,
    "created_at_timestamp": 1514407192,
    "edited_by_user_id": null,
    "edited_at_timestamp": 1514407192,
    "Name": "Bottle Sorting",
    "from_frab": 0,
    "map_url": "",
    "description": "The bottle sorting is situated in Hall 4 just next to the BOC."
  },
  {
    "shifttype_id": 32,
    "name": "Access Control",
    "id": 8494,
    "SID": 3447,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "Access Control",
    "start": 1514430000,
    "end": 1514437200,
    "RID": 59,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 11,
    "created_at_timestamp": 1514425071,
    "edited_by_user_id": null,
    "edited_at_timestamp": 1514425071,
    "Name": "CCL Merchandise (Level -1)",
    "from_frab": 0,
    "map_url": "https://34c3.c3nav.de/l/c:-1:239.04:353.05/@-1,236.77,358.65,4.1",
    "description": ""
  },
  {
    "shifttype_id": 32,
    "name": "Access Control",
    "id": 10495,
    "SID": 3553,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "",
    "start": 1514494800,
    "end": 1514502000,
    "RID": 60,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 1445,
    "created_at_timestamp": 1514476294,
    "edited_by_user_id": null,
    "edited_at_timestamp": 1514476294,
    "Name": "Hall 2 Transit North West",
    "from_frab": 0,
    "map_url": "https://34c3.c3nav.de/l/exhibition-hall-2-transit-north-west/",
    "description": "Emergency Exit"
  },
  {
    "shifttype_id": 52,
    "name": "Standby Angel",
    "id": 11326,
    "SID": 3611,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "Standby Angel, im Heaven melden",
    "start": 1514503800,
    "end": 1514511000,
    "RID": 2,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 1545,
    "created_at_timestamp": 1514502527,
    "edited_by_user_id": 1545,
    "edited_at_timestamp": 1514503586,
    "Name": "Heaven",
    "from_frab": 0,
    "map_url": "https://34c3.c3nav.de/embed/l/heaven",
    "description": "Heaven is located in the westernmost part of the CCL at Level 0 (Multipurpose areas 1/2).\n\nDer Himmel befindet sich im westlichen Teil des CCL auf Ebene 0 (Mehrzweckflächen 1/2)."
  },
  {
    "shifttype_id": 32,
    "name": "Access Control",
    "id": 11236,
    "SID": 3459,
    "TID": 4,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "Access Control",
    "start": 1514516400,
    "end": 1514523600,
    "RID": 59,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 11,
    "created_at_timestamp": 1514425071,
    "edited_by_user_id": null,
    "edited_at_timestamp": 1514425071,
    "Name": "CCL Merchandise (Level -1)",
    "from_frab": 0,
    "map_url": "https://34c3.c3nav.de/l/c:-1:239.04:353.05/@-1,236.77,358.65,4.1",
    "description": ""
  },
  {
    "shifttype_id": 34,
    "name": "LOC Helpdesk",
    "id": 10119,
    "SID": 3542,
    "TID": 16,
    "UID": 778,
    "freeloaded": 0,
    "Comment": "",
    "title": "LOC Helpdesk",
    "start": 1514545200,
    "end": 1514552400,
    "RID": 36,
    "URL": null,
    "PSID": null,
    "created_by_user_id": 223,
    "created_at_timestamp": 1514465766,
    "edited_by_user_id": null,
    "edited_at_timestamp": 1514465766,
    "Name": "LOC Helpdesk",
    "from_frab": 0,
    "map_url": "https://34c3.c3nav.de/embed/l/loc-helpdesk/",
    "description": "The LOC Helpdesk is located in Hall 4."
  }
]"""

  @Test
  fun parseMyShifts() {
    val bais = ByteArrayInputStream(angelshiftsJson.toByteArray(Charset.defaultCharset()))
    val angelshifts = AngelshiftsJsonParser.parse(bais, "demo")

    assertEquals(7, angelshifts?.shifts?.size)
  }

}
