package me.s3lph.flanharp

import android.support.test.runner.AndroidJUnit4
import me.s3lph.flanharp.dataproc.FahrplanJsonParser
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.io.ByteArrayInputStream
import java.nio.charset.Charset

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class FahrplanParserTest {

  private val fahrplanXml = """
<schedule>
  <version>Eidelstedter Bürgerhaus</version>
  <conference>
    <acronym>34c3</acronym>
    <title>34th Chaos Communication Congress</title>
    <start>2017-12-27</start>
    <end>2017-12-31</end>
    <days>4</days>
    <timeslot_duration>00:15</timeslot_duration>
    <base_url>//fahrplan.events.ccc.de/congress/2017/Fahrplan/</base_url>
  </conference>
  <day date='2017-12-27' end='2017-12-28T03:00:00+01:00' index='1' start='2017-12-27T10:00:00+01:00'>
    <room name='Saal Adams'>
      <event guid='b036385c-ec1a-44e5-ae48-af703ce9b5d3' id='9292'>
        <date>2017-12-27T11:00:00+01:00</date>
        <start>11:00</start>
        <duration>00:30</duration>
        <room>Saal Adams</room>
        <slug>34c3-9292-eroffnung_tuwat</slug>
        <url>//fahrplan.events.ccc.de/congress/2017/Fahrplan/events/9292.html</url>
        <recording>
          <license></license>
          <optout>false</optout>
        </recording>
        <title>Eröffnung: tuwat</title>
        <subtitle></subtitle>
        <track>CCC</track>
        <type>lecture</type>
        <language>de</language>
        <abstract>Daß sich mit Kleinkomputern trotzalledem sinnvolle Sachen machen lassen, die keine zentralisierten Großorganisationen erfordern, glauben wir.</abstract>
        <description>Daß die innere Sicherheit erst durch Komputereinsatz möglich wird, glauben die Mächtigen heute alle.

        Daß Komputer nicht streiken, setzt sich als Erkenntnis langsam auch bei mittleren Unternehmen durch.

        Daß durch Komputereinsatz das Telefon noch schöner wird, glaubt die Post heute mit ihrem Bildschirmtextsystem in “Feldversuchen” beweisen zu müssen.

        Daß der “personal computer” nun in Deutschland dem videogesättigten BMW Fahrer angedreht werden soll, wird durch die nun einsetzenden Anzeigenkampagnen klar.

        Daß sich mit Kleinkomputern trotzalledem sinnvolle Sachen machen lassen, die keine zentralisierten Großorganisationen erfordern, glauben wir.

        Damit wir als Komputerfrieks nicht länger unkoordiniert vor uns hinwuseln, tun wir wat und treffen uns am 27.12.17 in Leipzig, Seehausener Allee 1 (TAZ-Hauptgebäude) ab 11:00 Uhr.

        Wir reden über internationale Netzwerke – Kommunikationsrecht – Datenrecht (Wem gehören meine Daten?) – Copyright – Informations- u. Lernsysteme – Datenbanken – Encryption – Komputerspiele – Programmiersprachen – processcontrol – Hardware – und was auch immer.</description>
        <logo></logo>
        <persons>
          <person id='1817'>Tim Pritlove</person>
        </persons>
        <links>
        </links>
        <attachments>
        </attachments>
      </event>
      <event guid='2ef3b60f-6e5c-4c23-a145-d263685ec13e' id='9270'>
        <date>2017-12-27T11:30:00+01:00</date>
        <start>11:30</start>
        <duration>01:00</duration>
        <room>Saal Adams</room>
        <slug>34c3-9270-dude_you_broke_the_future</slug>
        <url>//fahrplan.events.ccc.de/congress/2017/Fahrplan/events/9270.html</url>
        <recording>
          <license></license>
          <optout>false</optout>
        </recording>
        <title>Dude, you broke the Future!</title>
        <subtitle></subtitle>
        <track>Art &amp; Culture</track>
        <type>lecture</type>
        <language>en</language>
        <abstract>We&#39;re living in yesterday&#39;s future, and it&#39;s nothing like the speculations of our authors and film/TV producers. As a working science fiction novelist, I take a professional interest in how we get predictions about the future wrong, and why, so that I can avoid repeating the same mistakes. Science fiction is written by people embedded within a society with expectations and political assumptions that bias us towards looking at the shiny surface of new technologies rather than asking how human beings will use them, and to taking narratives of progress at face value rather than asking what hidden agenda they serve.

        In this talk, author Charles Stross will give a rambling, discursive, and angry tour of what went wrong with the 21st century, why we didn&#39;t see it coming, where we can expect it to go next, and a few suggestions for what to do about it if we don&#39;t like it.
        </abstract>
        <description>We&#39;re living in yesterday&#39;s future, and it&#39;s nothing like the speculations of our authors and film/TV producers. As a working science fiction novelist, I take a professional interest in how we get predictions about the future wrong, and why, so that I can avoid repeating the same mistakes. Science fiction is written by people embedded within a society with expectations and political assumptions that bias us towards looking at the shiny surface of new technologies rather than asking how human beings will use them, and to taking narratives of progress at face value rather than asking what hidden agenda they serve.

        In this talk, author Charles Stross will give a rambling, discursive, and angry tour of what went wrong with the 21st century, why we didn&#39;t see it coming, where we can expect it to go next, and a few suggestions for what to do about it if we don&#39;t like it.
        </description>
        <logo></logo>
        <persons>
          <person id='7999'>Charles Stross</person>
        </persons>
        <links>
        </links>
        <attachments>
        </attachments>
      </event>
      <event guid='da934433-0092-4749-b606-56b65e84214f' id='9092'>
        <date>2017-12-27T12:45:00+01:00</date>
        <start>12:45</start>
        <duration>01:00</duration>
        <room>Saal Adams</room>
        <slug>34c3-9092-ladeinfrastruktur_fur_elektroautos_ausbau_statt_sicherheit</slug>
        <url>//fahrplan.events.ccc.de/congress/2017/Fahrplan/events/9092.html</url>
        <recording>
          <license>CC BY 4.0</license>
          <optout>false</optout>
        </recording>
        <title>Ladeinfrastruktur für Elektroautos: Ausbau statt Sicherheit</title>
        <subtitle>Warum das Laden eines Elektroautos unsicher ist</subtitle>
        <track>Security</track>
        <type>lecture</type>
        <language>de</language>
        <abstract>Wir retten das Klima mit Elektroautos — und bauen die Ladeinfrastruktur massiv aus. Leider werden dabei auch Schwachstellen auf allen Ebenen sichtbar: Von fehlender Manipulationssicherheit der Ladesäulen bis hin zu inhärent unsicheren Zahlungsprotokollen und kopierbaren Zahlkarten. Ladesäulenhersteller und Ladenetzbetreiber lassen ihre Kunden im Regen stehen — geht das schnelle Wachstum des Marktanteils zu Lasten der Kundensicherheit?</abstract>
        <description>Eine (AC-)Ladesäule ist eigentlich nur eine glorifizierte Drehstromsteckdose. Mit einem Autosimulator (vgl. https://evsim.gonium.net) kann man auf vielen Parkplätzen Strom beziehen, zum Beispiel um Waffeln zu backen:

        https://www.youtube.com/watch?v=pUEp3uWAWqY

        Mit diesem Simulator habe ich mir verschiedene Ladesäulen sowie ihre Backend-Kommunikation angeschaut. An den meisten Ladesäulen im öffentlichen Raum weist man sich mittels NFC-Chipkarte aus. Über das “Open Charge Point Protocol” (OCPP)  (vgl. http://www.openchargealliance.org/protocols/ocpp/ocpp-15/) redet die Ladesäule dann mit einem Backend und prüft, ob der Ladevorgang freigeschaltet werden darf. Leider weisen sowohl die verwendeten Chipkarte als auch das OCPP-Protokoll selbst gravierende Mängel auf:

        Es ist mit geringen Aufwand möglich, auf fremde Kosten zu laden.
        Böswillige Ladesäulenbetreiber könnten Ladevorgänge protokollieren und später “virtuelle” Ladevorgänge simulieren, um zusätzlichen Umsatz zu generieren.

        Ladesäulen sind teilweise über das Internet erreichbar und können ferngesteuert werden: Ein laufender Ladevorgang kann aus der Ferne abgebrochen werden.

        Wer physischen Zugriff auf Ladestationen hat kann diese beliebig umkonfigurieren und so z.B. alle Informationen für das Klonen von Ladekarten abschnorcheln. Der Vortrag stellt die Funktionsweise der Abrechnungssysteme dar und zeigt Proof of Concept-Implementationen verschiedener Angriffe.</description>
        <logo></logo>
        <persons>
          <person id='2461'>Mathias Dalheimer</person>
        </persons>
        <links>
          <link href='https://www.youtube.com/watch?v=pUEp3uWAWqY'>Waffeln an der Elektrotankstelle backen</link>
          <link href='https://evsim.gonium.net/'>EVSim: Ein einfacher Elektroauto-Simulator</link>
        </links>
        <attachments>
        </attachments>
      </event>
    </room>
  </day>
</schedule>"""

  @Test
  fun parseReduced34c3Fahrplan() {
    val bais = ByteArrayInputStream(fahrplanXml.toByteArray(Charset.defaultCharset()))
    val fahrplan = FahrplanJsonParser.parse(bais, "demo")

    assertEquals("34th Chaos Communication Congress", fahrplan?.conference?.title)
    assertEquals("34c3", fahrplan?.conference?.acronym)
    assertEquals("Wed Dec 27 00:00:00 GMT+01:00 2017", fahrplan?.conference?.start.toString())
    assertEquals("Sun Dec 31 00:00:00 GMT+01:00 2017", fahrplan?.conference?.end.toString())
    assertEquals("Eidelstedter Bürgerhaus", fahrplan?.version)
    assertEquals(3, fahrplan?.events?.size)

    assertEquals("Eröffnung: tuwat", fahrplan?.events?.get(0)?.title)
    assertEquals("Wed Dec 27 11:00:00 GMT+01:00 2017", fahrplan?.events?.get(0)?.start.toString())
    assertEquals("Wed Dec 27 11:30:00 GMT+01:00 2017", fahrplan?.events?.get(0)?.end.toString())
    assertEquals("Saal Adams", fahrplan?.events?.get(0)?.room)
    assertEquals("CCC", fahrplan?.events?.get(0)?.track)
    assertEquals("lecture", fahrplan?.events?.get(0)?.type)
    assertEquals("//fahrplan.events.ccc.de/congress/2017/Fahrplan/events/9292.html", fahrplan?.events?.get(0)?.url.toString())
  }

}
