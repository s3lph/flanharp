# FlanHarp

Android app for offline storage of [frab][frab] conference schedules.

## Status

This project was my "playground" for learning Kotlin.  It is neither documented nor actively
maintained.  Yet from time to time I put some work into it.

## Features

- Offline storage of frab schedules.
- Support for [Engelsystem][engel] subscriptions.
- Bookmark conference events and receive reminder notifications 15 minutes in advance.
- Directly open a talk's recording in [media.ccc.de][media].  (I haven't yet looked into
  reconstructing the live stream link from the event slug, though).

## Authors

- s3lph

[frab]: https://github.com/frab/frab
[engel]: https://engelsystem.de/
[media]: https://media.ccc.de/
